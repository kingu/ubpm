#ifndef DLGHELP_H
#define DLGHELP_H

#include "MainWindow.h"
#include "ui_DialogHelp.h"

class DialogHelp : public QDialog, private Ui::DialogHelp
{
	Q_OBJECT

public:

	explicit DialogHelp(QWidget*, QString);
	~DialogHelp();

	void setSourceFromPage(QString);

private:

	QString language;

	QHelpEngine *helpEngine;
	QHelpContentWidget *contentWidget;

private slots:

	void initAfterShown(QString);

	void setSource(QUrl);
	void setSourceFromContent(QModelIndex);

	void anchorClicked(QUrl);
};

#endif // DLGHELP_H
