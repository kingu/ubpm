<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="el">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Ανάλυση Δεδομένων</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Ερώτημα</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="203"/>
        <source>Results</source>
        <translation>Αποτελέσματα</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Ημερομηνία</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Ώρα</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Κίνηση</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Μη ορατό</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Σχόλιο</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Κλείσετε</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="205"/>
        <source>No results for this query found!</source>
        <translation>Δεν βρέθηκαν αποτελέσματα για αυτό το ερώτημα!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 για %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Αποτέλεσμα</numerusform>
            <numerusform>Αποτελέσματα</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Match(es)</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Record(s)</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>User %1</source>
        <translation>Χρήστης %1</translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Οδηγός</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="19"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="56"/>
        <source>%1 guide not found!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Μη αυτόματη εγγραφή</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Εγγραφή δεδομένων</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="220"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Προσθήκη εγγραφής για %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Επιλέξτε ημερομηνία και ώρα</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="151"/>
        <source>Enter DIA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="173"/>
        <source>Enter BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="195"/>
        <source>Movement</source>
        <translation>Κίνηση</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="248"/>
        <source>Enter Optional Comment</source>
        <translation>Εισαγάγετε προαιρετικό σχόλιο</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="278"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Εμφάνιση μηνύματος για επιτυχημένη δημιουργία / διαγραφή / τροποποίηση εγγραφής</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="296"/>
        <source>Delete</source>
        <translation>Διαγράψετε</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="313"/>
        <source>Create</source>
        <translation>Δημιουργία</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="333"/>
        <source>Close</source>
        <translation>Κλείσετε</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Χρήστης 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Χρήστης 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="30"/>
        <location filename="../DialogRecord.cpp" line="99"/>
        <source>Modify</source>
        <translation>Τροποποίηση</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="46"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="52"/>
        <source>Data record successfully deleted.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="62"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="72"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="81"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="103"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="109"/>
        <source>Data Record successfully modified.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="118"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="124"/>
        <source>Data Record successfully created.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.cpp" line="139"/>
        <source>Choose Database Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="175"/>
        <source>Could not display manual!

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="302"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="309"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="316"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="323"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="330"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="338"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="345"/>
        <source>Please enter a valid e-mail address!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="352"/>
        <source>Please enter a e-mail subject!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="359"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="452"/>
        <source>User 1</source>
        <translation type="unfinished">Χρήστης 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="466"/>
        <source>User 2</source>
        <translation type="unfinished">Χρήστης 2</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="579"/>
        <source>Blood Pressure Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="580"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="612"/>
        <source>Abort setup and discard all changes?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="50"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="56"/>
        <source>Current Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="72"/>
        <source>Change Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="98"/>
        <source>Encrypt With SQLCipher</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="101"/>
        <source>Encryption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="113"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Show Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="154"/>
        <source>User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="189"/>
        <location filename="../DialogSettings.ui" line="440"/>
        <source>Male</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="218"/>
        <location filename="../DialogSettings.ui" line="469"/>
        <source>Female</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="308"/>
        <location filename="../DialogSettings.ui" line="568"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="183"/>
        <location filename="../DialogSettings.ui" line="434"/>
        <source>Mandatory Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="244"/>
        <location filename="../DialogSettings.ui" line="504"/>
        <source>Age Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="324"/>
        <location filename="../DialogSettings.ui" line="584"/>
        <source>Additional Information</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <location filename="../DialogSettings.ui" line="638"/>
        <source>Height</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="407"/>
        <location filename="../DialogSettings.ui" line="667"/>
        <source>Weight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="692"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="698"/>
        <source>Import Plugins</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="722"/>
        <source>Show Device Image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="745"/>
        <source>Show Device Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Open Website</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="803"/>
        <source>Maintainer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="831"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="838"/>
        <source>Send E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="851"/>
        <source>Model</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="858"/>
        <source>Producer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="875"/>
        <source>Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1002"/>
        <source>X-Axis Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1008"/>
        <source>Dynamic Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Healthy Ranges</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="906"/>
        <source>Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="983"/>
        <source>Colored Areas</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="352"/>
        <location filename="../DialogSettings.ui" line="612"/>
        <source>Birth Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="707"/>
        <source>Please choose Device Plugin…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1130"/>
        <location filename="../DialogSettings.ui" line="1405"/>
        <source>Systolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1218"/>
        <location filename="../DialogSettings.ui" line="1493"/>
        <source>Diastolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1306"/>
        <location filename="../DialogSettings.ui" line="1581"/>
        <source>Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1678"/>
        <source>Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1763"/>
        <location filename="../DialogSettings.ui" line="1946"/>
        <source>Systolic Warnlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1709"/>
        <location filename="../DialogSettings.ui" line="2006"/>
        <source>Diastolic Warnlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="915"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="939"/>
        <location filename="../DialogSettings.ui" line="967"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1021"/>
        <source>Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1030"/>
        <location filename="../DialogSettings.ui" line="1058"/>
        <source>Width</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1087"/>
        <source>Show Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1093"/>
        <source>Show Heartrate in Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1103"/>
        <source>Print Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1109"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1823"/>
        <location filename="../DialogSettings.ui" line="2064"/>
        <source>P.Pressure Warnlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1877"/>
        <location filename="../DialogSettings.ui" line="2118"/>
        <source>Heartrate Warnlevel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2183"/>
        <source>Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2189"/>
        <source>Bar Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2195"/>
        <source>Show Median instead of Average Bars</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2205"/>
        <source>Legend Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2211"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2226"/>
        <source>E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2234"/>
        <source>Address</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2253"/>
        <source>Subject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2271"/>
        <source>Message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2292"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2298"/>
        <source>Autostart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2304"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2317"/>
        <source>Notification</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2323"/>
        <source>Always show Result after Online Update Check</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2339"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2356"/>
        <source>Reset</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2373"/>
        <source>Close</source>
        <translation type="unfinished">Κλείσετε</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="196"/>
        <source>Download</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="216"/>
        <source>Ignore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="162"/>
        <source>No new version found.</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="135"/>
        <source>Unexpected response from update server!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="182"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="186"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="199"/>
        <source>Could not start new version!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="242"/>
        <source>Really abort download?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="206"/>
        <source>Could not save update to %1!

%2</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="20"/>
        <source>Universal Blood Pressure Manager</source>
        <translation type="unfinished">Καθολικός διευθυντής πίεσης αίματος</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation type="unfinished">Ημερομηνία</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation type="unfinished">Ώρα</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="3449"/>
        <source>Systolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="78"/>
        <location filename="../MainWindow.cpp" line="3450"/>
        <source>Diastolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation type="unfinished">Κίνηση</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation type="unfinished">Μη ορατό</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation type="unfinished">Σχόλιο</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1196"/>
        <source>Database</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1200"/>
        <source>Import</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1212"/>
        <source>Export</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1307"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1310"/>
        <location filename="../MainWindow.ui" line="1313"/>
        <source>Quit Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1325"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1328"/>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>About Program</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1340"/>
        <source>Guide</source>
        <translation type="unfinished">Οδηγός</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1343"/>
        <location filename="../MainWindow.ui" line="1346"/>
        <source>Show Guide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1358"/>
        <location filename="../MainWindow.ui" line="1361"/>
        <source>Check Update</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1752"/>
        <source>PayPal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1755"/>
        <location filename="../MainWindow.ui" line="1758"/>
        <source>Donate via PayPal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1770"/>
        <location filename="../MainWindow.ui" line="1773"/>
        <source>Donate via Liberapay</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1785"/>
        <location filename="../MainWindow.ui" line="1788"/>
        <source>Donate via Amazon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1800"/>
        <location filename="../MainWindow.ui" line="1803"/>
        <source>Donate via SEPA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <source>Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1815"/>
        <location filename="../MainWindow.ui" line="1818"/>
        <source>Contribute Translation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <source>E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1830"/>
        <location filename="../MainWindow.ui" line="1833"/>
        <source>Send E-Mail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="79"/>
        <location filename="../MainWindow.cpp" line="3451"/>
        <source>Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1373"/>
        <source>Bugreport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1376"/>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Send Bugreport</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1388"/>
        <source>Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1391"/>
        <location filename="../MainWindow.ui" line="1394"/>
        <source>Change Settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1409"/>
        <source>From Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <location filename="../MainWindow.ui" line="1415"/>
        <source>Import From Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>From File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Import From File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1442"/>
        <source>From Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <location filename="../MainWindow.ui" line="1448"/>
        <source>Import From Input</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>To CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <location filename="../MainWindow.ui" line="1463"/>
        <source>Export To CSV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1475"/>
        <source>To XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1478"/>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>Export To XML</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1490"/>
        <source>To JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1493"/>
        <location filename="../MainWindow.ui" line="1496"/>
        <source>Export To JSON</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>To SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1508"/>
        <location filename="../MainWindow.ui" line="1511"/>
        <source>Export To SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Print Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1523"/>
        <location filename="../MainWindow.ui" line="1526"/>
        <source>Print Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Print Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1538"/>
        <location filename="../MainWindow.ui" line="1541"/>
        <source>Print Table View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Print Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1553"/>
        <location filename="../MainWindow.ui" line="1556"/>
        <source>Print Statistic View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Preview Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1568"/>
        <location filename="../MainWindow.ui" line="1571"/>
        <source>Preview Chart View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <source>Preview Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1583"/>
        <location filename="../MainWindow.ui" line="1586"/>
        <source>Preview Table View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <source>Preview Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1598"/>
        <location filename="../MainWindow.ui" line="1601"/>
        <source>Preview Statistic View</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <location filename="../MainWindow.ui" line="1616"/>
        <source>Clear All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <location filename="../MainWindow.ui" line="1634"/>
        <source>Clear User 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <location filename="../MainWindow.ui" line="1649"/>
        <source>Clear User 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1709"/>
        <source>Analysis</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1712"/>
        <location filename="../MainWindow.ui" line="1715"/>
        <source>Analyze Records</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1734"/>
        <location filename="../MainWindow.ui" line="1737"/>
        <location filename="../MainWindow.ui" line="1740"/>
        <source>Time Mode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="49"/>
        <location filename="../MainWindow.cpp" line="50"/>
        <location filename="../MainWindow.cpp" line="3419"/>
        <location filename="../MainWindow.cpp" line="3420"/>
        <source>Records For Selected User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="57"/>
        <location filename="../MainWindow.cpp" line="58"/>
        <location filename="../MainWindow.cpp" line="3422"/>
        <location filename="../MainWindow.cpp" line="3423"/>
        <source>Select Date &amp; Time</source>
        <translation type="unfinished">Επιλέξτε ημερομηνία και ώρα</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="87"/>
        <location filename="../MainWindow.cpp" line="3453"/>
        <source>Systolic - Value Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="88"/>
        <location filename="../MainWindow.cpp" line="3454"/>
        <source>Diastolic - Value Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="91"/>
        <location filename="../MainWindow.cpp" line="3456"/>
        <source>Systolic - Target Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="92"/>
        <location filename="../MainWindow.cpp" line="3457"/>
        <source>Diastolic - Target Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1176"/>
        <source>Athlete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1176"/>
        <source>To Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1177"/>
        <source>Excellent</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1177"/>
        <source>Optimal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1178"/>
        <source>Great</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1179"/>
        <source>Good</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1178"/>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1694"/>
        <location filename="../MainWindow.ui" line="1697"/>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="45"/>
        <location filename="../MainWindow.cpp" line="46"/>
        <location filename="../MainWindow.cpp" line="3261"/>
        <location filename="../MainWindow.cpp" line="3262"/>
        <location filename="../MainWindow.cpp" line="3263"/>
        <location filename="../MainWindow.cpp" line="3264"/>
        <location filename="../MainWindow.cpp" line="3483"/>
        <location filename="../MainWindow.cpp" line="3484"/>
        <location filename="../MainWindow.cpp" line="3485"/>
        <location filename="../MainWindow.cpp" line="3486"/>
        <source>Switch To %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="251"/>
        <location filename="../MainWindow.cpp" line="3261"/>
        <location filename="../MainWindow.cpp" line="3262"/>
        <location filename="../MainWindow.cpp" line="3483"/>
        <location filename="../MainWindow.cpp" line="3484"/>
        <source>User 1</source>
        <translation type="unfinished">Χρήστης 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="45"/>
        <location filename="../MainWindow.cpp" line="46"/>
        <location filename="../MainWindow.cpp" line="261"/>
        <location filename="../MainWindow.cpp" line="3263"/>
        <location filename="../MainWindow.cpp" line="3264"/>
        <location filename="../MainWindow.cpp" line="3485"/>
        <location filename="../MainWindow.cpp" line="3486"/>
        <source>User 2</source>
        <translation type="unfinished">Χρήστης 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="89"/>
        <location filename="../MainWindow.cpp" line="3455"/>
        <source>Heartrate - Value Range</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="93"/>
        <location filename="../MainWindow.cpp" line="3458"/>
        <source>Heartrate - Target Area</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="314"/>
        <source>Blood Pressure Report</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="315"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="712"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="717"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1179"/>
        <source>High Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1180"/>
        <location filename="../MainWindow.cpp" line="1650"/>
        <location filename="../MainWindow.cpp" line="3462"/>
        <location filename="../MainWindow.cpp" line="3466"/>
        <location filename="../MainWindow.cpp" line="3470"/>
        <location filename="../MainWindow.h" line="294"/>
        <location filename="../MainWindow.h" line="298"/>
        <location filename="../MainWindow.h" line="302"/>
        <source>Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1180"/>
        <source>Hyper 1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1181"/>
        <source>Below Average</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1181"/>
        <source>Hyper 2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1182"/>
        <source>Poor</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1182"/>
        <source>Hyper 3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1331"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1344"/>
        <location filename="../MainWindow.cpp" line="1366"/>
        <location filename="../MainWindow.cpp" line="3429"/>
        <source>Switch Language to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1391"/>
        <location filename="../MainWindow.cpp" line="1406"/>
        <location filename="../MainWindow.cpp" line="3437"/>
        <source>Switch Theme to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1431"/>
        <location filename="../MainWindow.cpp" line="1451"/>
        <location filename="../MainWindow.cpp" line="3445"/>
        <source>Switch Style to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1522"/>
        <location filename="../MainWindow.cpp" line="1547"/>
        <source>Edit record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1650"/>
        <location filename="../MainWindow.cpp" line="3463"/>
        <location filename="../MainWindow.cpp" line="3467"/>
        <location filename="../MainWindow.cpp" line="3471"/>
        <location filename="../MainWindow.h" line="295"/>
        <location filename="../MainWindow.h" line="299"/>
        <location filename="../MainWindow.h" line="303"/>
        <source>Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1652"/>
        <source>Click to swap Average and Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1747"/>
        <source>Click to swap Legend and Label</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1824"/>
        <source>No records to preview for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1856"/>
        <source>No records to print for selected time range!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1894"/>
        <location filename="../MainWindow.cpp" line="1944"/>
        <location filename="../MainWindow.cpp" line="2018"/>
        <location filename="../MainWindow.cpp" line="2136"/>
        <source>%1 (Age: %2, Height: %3, Weight: %4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1894"/>
        <location filename="../MainWindow.cpp" line="1944"/>
        <location filename="../MainWindow.cpp" line="2018"/>
        <location filename="../MainWindow.cpp" line="2136"/>
        <source>User %1</source>
        <translation type="unfinished">Χρήστης %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>DATE</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>TIME</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>SYS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>DIA</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>BPM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>IHB</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>COMMENT</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>PPR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2200"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3186"/>
        <source>Chart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3203"/>
        <source>Table</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3220"/>
        <source>Statistic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3238"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3248"/>
        <source>Could not start e-mail client!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3334"/>
        <source>Thanks to all translators:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3334"/>
        <source>Version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3354"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3371"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3396"/>
        <source>Loading application translation failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1888"/>
        <location filename="../MainWindow.cpp" line="1938"/>
        <location filename="../MainWindow.cpp" line="2012"/>
        <location filename="../MainWindow.cpp" line="2130"/>
        <source>Created with UBPM for
Windows / Linux / MacOS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1889"/>
        <location filename="../MainWindow.cpp" line="1939"/>
        <location filename="../MainWindow.cpp" line="2013"/>
        <location filename="../MainWindow.cpp" line="2131"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2263"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2263"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2326"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="711"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="716"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation type="unfinished"></translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2301"/>
        <location filename="../MainWindow.cpp" line="3078"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2305"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2310"/>
        <location filename="../MainWindow.cpp" line="3082"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform></numerusform>
            <numerusform></numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2696"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2745"/>
        <source>Export to %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2745"/>
        <source>%1 File (*.%2)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2776"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2782"/>
        <source>The database is empty, no records to export!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2940"/>
        <source>Morning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2940"/>
        <source>Afternoon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2948"/>
        <source>Week</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2956"/>
        <source>Quarter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2960"/>
        <source>Half Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2964"/>
        <source>Year</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3012"/>
        <source>Really delete all records for user %1?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3027"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3132"/>
        <source>Really delete all records?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3143"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3334"/>
        <source>This program may be installed and used free of charge for non-commercial use on as many computers as you like without limitations. A liability for any damages resulting from the use is excluded. Use at your own risk.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3460"/>
        <location filename="../MainWindow.cpp" line="3464"/>
        <location filename="../MainWindow.cpp" line="3468"/>
        <location filename="../MainWindow.h" line="292"/>
        <location filename="../MainWindow.h" line="296"/>
        <location filename="../MainWindow.h" line="300"/>
        <source>Minimum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3461"/>
        <location filename="../MainWindow.cpp" line="3465"/>
        <location filename="../MainWindow.cpp" line="3469"/>
        <location filename="../MainWindow.h" line="293"/>
        <location filename="../MainWindow.h" line="297"/>
        <location filename="../MainWindow.h" line="301"/>
        <source>Maximum</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3519"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <location filename="../MainWindow.cpp" line="1536"/>
        <location filename="../MainWindow.cpp" line="4148"/>
        <source>Delete record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4071"/>
        <source>Show Lines</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4075"/>
        <source>Show Heartrate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4111"/>
        <location filename="../MainWindow.cpp" line="4122"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4150"/>
        <source>Show record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <location filename="../MainWindow.cpp" line="1543"/>
        <location filename="../MainWindow.cpp" line="4151"/>
        <source>Hide record</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1538"/>
        <location filename="../MainWindow.cpp" line="4162"/>
        <source>Really delete selected record?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3405"/>
        <source>Loading Qt base translation for &quot;%1&quot; failed!

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4067"/>
        <source>Dynamic Scaling</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4068"/>
        <source>Colored Stripes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4070"/>
        <source>Show Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4073"/>
        <source>Colored Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4183"/>
        <source>Show Median</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4184"/>
        <source>Show Values</source>
        <translation>Εμφάνιση τιμών</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4225"/>
        <source>Really quit program?</source>
        <translation>Πραγματικά εγκαταλείπετε το πρόγραμμα;</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Καθολικός διευθυντής πίεσης αίματος</translation>
    </message>
</context>
</TS>
