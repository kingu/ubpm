<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="es_ES">
<context>
    <name>DialogAnalysis</name>
    <message>
        <location filename="../DialogAnalysis.ui" line="20"/>
        <source>Data Analysis</source>
        <translation>Análisis de datos</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="30"/>
        <source>Query</source>
        <translation>Consulta</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="49"/>
        <location filename="../DialogAnalysis.cpp" line="203"/>
        <source>Results</source>
        <translation>Resultados</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="88"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="97"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="106"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="115"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="124"/>
        <source>P.Pressure</source>
        <translation>Presión de pulso</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="133"/>
        <source>Heartrate</source>
        <translation>Frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="142"/>
        <source>Irregular</source>
        <translation>F. cardíaca irregular</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="151"/>
        <source>Movement</source>
        <translation>Movimiento durante la toma</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="160"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="169"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.ui" line="199"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="48"/>
        <source>Could not create memory database!

%1</source>
        <translation>¡No se pudo crear la base de datos!

%1</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="205"/>
        <source>No results for this query found!</source>
        <translation>¡No se han encontrado resultados para esta consulta!</translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>%1 for %2 [ %3 %4 | %5 %6 | %7% ]</source>
        <translation>%1 para %2 [ %3 %4 | %5 %6 | %7% ]</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Result(s)</source>
        <translation>
            <numerusform>Resultado</numerusform>
            <numerusform>Resultados</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>User %1</source>
        <translation>Usuario %1</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Match(es)</source>
        <translation>
            <numerusform>Coincidencia</numerusform>
            <numerusform>Coincidencias</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogAnalysis.cpp" line="211"/>
        <source>Record(s)</source>
        <translation>
            <numerusform>Registro</numerusform>
            <numerusform>Registros</numerusform>
        </translation>
    </message>
</context>
<context>
    <name>DialogHelp</name>
    <message>
        <location filename="../DialogHelp.ui" line="20"/>
        <source>Guide</source>
        <translation>Guía</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="19"/>
        <source>%1 guide not found, showing EN guide instead.</source>
        <translation>Guía %1 no encontrada, mostrando guía EN en su lugar.</translation>
    </message>
    <message>
        <location filename="../DialogHelp.cpp" line="56"/>
        <source>%1 guide not found!</source>
        <translation>¡Guía %1 no encontrada!</translation>
    </message>
</context>
<context>
    <name>DialogRecord</name>
    <message>
        <location filename="../DialogRecord.ui" line="14"/>
        <source>Manual Record</source>
        <translation>Registro manual</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="27"/>
        <source>Data Record</source>
        <translation>Registro de datos</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="35"/>
        <location filename="../DialogRecord.ui" line="220"/>
        <location filename="../DialogRecord.cpp" line="9"/>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>Add Record For %1</source>
        <translation>Añadir registro para %1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="78"/>
        <source>Select Date &amp; Time</source>
        <translation>Seleccionar fecha y hora</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="102"/>
        <source>Irregular Heartbeat</source>
        <translation>Pulso irregular</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="129"/>
        <source>Enter SYS</source>
        <translation>Introduzca la presión sistólica</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="151"/>
        <source>Enter DIA</source>
        <translation>Introduzca la presión diastólica</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="173"/>
        <source>Enter BPM</source>
        <translation>Introduzca la frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="195"/>
        <source>Movement</source>
        <translation>Movimiento</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="248"/>
        <source>Enter Optional Comment</source>
        <translation>Introduzca un comentario opcional</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="278"/>
        <source>Show message on successful create / delete / modify record</source>
        <translation>Mostrar un mensaje tras la creación, modificación o eliminación exitosa de un registro</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="296"/>
        <source>Delete</source>
        <translation>Eliminar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="313"/>
        <source>Create</source>
        <translation>Crear</translation>
    </message>
    <message>
        <location filename="../DialogRecord.ui" line="333"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="9"/>
        <source>User 1</source>
        <translation>Usuario 1</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="10"/>
        <source>User 2</source>
        <translation>Usuario 2</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="30"/>
        <location filename="../DialogRecord.cpp" line="99"/>
        <source>Modify</source>
        <translation>Modificar</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="46"/>
        <source>The data record could not be deleted!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>¡No se pudo eliminar el registro!

No existe ningún registro con esta fecha y hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="52"/>
        <source>Data record successfully deleted.</source>
        <translation>Registr eliminado con éxito.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="62"/>
        <source>Please enter a valid value for &quot;SYS&quot; first!</source>
        <translation>¡Introduzca un valor válido para la presión sistólica!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="72"/>
        <source>Please enter a valid value for &quot;DIA&quot; first!</source>
        <translation>¡Introduzca un valor válido para la presión diastólica!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="81"/>
        <source>Please enter a valid value for &quot;BPM&quot; first!</source>
        <translation>¡Introduzca un valor válido para la frecuencia cardíaca!</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="103"/>
        <source>The data record could not be modified!

An entry for this date &amp; time doesn&apos;t exist.</source>
        <translation>¡No se pudo modificar el registro!

No existe ningún registro con esta fecha y hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="109"/>
        <source>Data Record successfully modified.</source>
        <translation>Registr modificado con éxito.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="118"/>
        <source>The data record could not be created!

An entry for this date &amp; time already exist.</source>
        <translation>¡No se pudo crear el registro!

Ya existe un registro con esta fecha y hora.</translation>
    </message>
    <message>
        <location filename="../DialogRecord.cpp" line="124"/>
        <source>Data Record successfully created.</source>
        <translation>Registr creado con éxito.</translation>
    </message>
</context>
<context>
    <name>DialogSettings</name>
    <message>
        <location filename="../DialogSettings.ui" line="20"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="44"/>
        <source>Database</source>
        <translation>Base de datos</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="50"/>
        <source>Location</source>
        <translation>Ubicación</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="56"/>
        <source>Current Location</source>
        <translation>Ubicación actual</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="72"/>
        <source>Change Location</source>
        <translation>Cambiar ubicación</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="98"/>
        <source>Encrypt With SQLCipher</source>
        <translation>Encriptar con SQLCipher</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="101"/>
        <source>Encryption</source>
        <translation>Encriptación</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="113"/>
        <source>Password</source>
        <translation>Contraseña</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="126"/>
        <source>Show Password</source>
        <translation>Mostrar contraseña</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="154"/>
        <source>User</source>
        <translation>Usuario</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="183"/>
        <location filename="../DialogSettings.ui" line="434"/>
        <source>Mandatory Information</source>
        <translation>Información necesaria</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="189"/>
        <location filename="../DialogSettings.ui" line="440"/>
        <source>Male</source>
        <translation>Hombre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="218"/>
        <location filename="../DialogSettings.ui" line="469"/>
        <source>Female</source>
        <translation>Mujer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="244"/>
        <location filename="../DialogSettings.ui" line="504"/>
        <source>Age Group</source>
        <translation>Grupo de edad</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="308"/>
        <location filename="../DialogSettings.ui" line="568"/>
        <source>Name</source>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="324"/>
        <location filename="../DialogSettings.ui" line="584"/>
        <source>Additional Information</source>
        <translation>Información adicional</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="352"/>
        <location filename="../DialogSettings.ui" line="612"/>
        <source>Birth Year</source>
        <translation>Año de nacimiento</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="378"/>
        <location filename="../DialogSettings.ui" line="638"/>
        <source>Height</source>
        <translation>Altura</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="407"/>
        <location filename="../DialogSettings.ui" line="667"/>
        <source>Weight</source>
        <translation>Peso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="692"/>
        <source>Device</source>
        <translation>Dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="698"/>
        <source>Import Plugins</source>
        <translation>Complementos de importación</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="707"/>
        <source>Please choose Device Plugin…</source>
        <translation>Por favor, elija un complemento de dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="722"/>
        <source>Show Device Image</source>
        <translation>Mostrar imagen del dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="745"/>
        <source>Show Device Manual</source>
        <translation>Mostrar manual del dispositivo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="790"/>
        <source>Open Website</source>
        <translation>Abrir página web</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="803"/>
        <source>Maintainer</source>
        <translation>Mantenedor</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="831"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="838"/>
        <source>Send E-Mail</source>
        <translation>Enviar mensaje por correo electrónico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="851"/>
        <source>Model</source>
        <translation>Modelo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="858"/>
        <source>Producer</source>
        <translation>Fabricante</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="875"/>
        <source>Chart</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="906"/>
        <source>Symbols</source>
        <translation>Símbolos</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="915"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="939"/>
        <location filename="../DialogSettings.ui" line="967"/>
        <source>Size</source>
        <translation>Tamaño</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="983"/>
        <source>Colored Areas</source>
        <translation>Áreas de colores</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="989"/>
        <source>Healthy Ranges</source>
        <translation>Rangos saludables</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1002"/>
        <source>X-Axis Range</source>
        <translation>Rango del eje X</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1008"/>
        <source>Dynamic Scaling</source>
        <translation>Escalado dinámico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1021"/>
        <source>Lines</source>
        <translation>Líneas</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1030"/>
        <location filename="../DialogSettings.ui" line="1058"/>
        <source>Width</source>
        <translation>Ancho</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1087"/>
        <source>Show Heartrate</source>
        <translation>Mostrar frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1093"/>
        <source>Show Heartrate in Chart View</source>
        <translation>Mostrar frecuencia cardíaca en la vista de gráfico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1103"/>
        <source>Print Heartrate</source>
        <translation>Imprimir frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1109"/>
        <source>Print Heartrate on separate Sheet</source>
        <translation>Imprimir frecuencia cardíaca en una página separada</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1130"/>
        <location filename="../DialogSettings.ui" line="1405"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1218"/>
        <location filename="../DialogSettings.ui" line="1493"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1306"/>
        <location filename="../DialogSettings.ui" line="1581"/>
        <source>Heartrate</source>
        <translation>Frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1678"/>
        <source>Table</source>
        <translation>Tabla</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1709"/>
        <location filename="../DialogSettings.ui" line="2006"/>
        <source>Diastolic Warnlevel</source>
        <translation>Nivel de aviso para la presión diastólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1763"/>
        <location filename="../DialogSettings.ui" line="1946"/>
        <source>Systolic Warnlevel</source>
        <translation>Nivel de aviso para la presión sistólica</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1823"/>
        <location filename="../DialogSettings.ui" line="2064"/>
        <source>P.Pressure Warnlevel</source>
        <translation>Nivel de aviso para la presión de pulso</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="1877"/>
        <location filename="../DialogSettings.ui" line="2118"/>
        <source>Heartrate Warnlevel</source>
        <translation>Nivel de aviso para la frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2183"/>
        <source>Statistic</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2189"/>
        <source>Bar Type</source>
        <translation>Tipo de barra</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2195"/>
        <source>Show Median instead of Average Bars</source>
        <translation>Mostrar barras de mediana en vez de barras de media</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2205"/>
        <source>Legend Type</source>
        <translation>Tipo de leyenda</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2211"/>
        <source>Show Values as Legend instead of Descriptions</source>
        <translation>Mostrar values como leyendas en vez de como descripciones</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2226"/>
        <source>E-Mail</source>
        <translation>Correo electrónico</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2234"/>
        <source>Address</source>
        <translation>Dirección de correo</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2253"/>
        <source>Subject</source>
        <translation>Asunto</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2271"/>
        <source>Message</source>
        <translation>Mensaje</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2292"/>
        <source>Update</source>
        <translation>Actualizaciones</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2298"/>
        <source>Autostart</source>
        <translation>Inicio</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2304"/>
        <source>Check for Online Updates at Program Startup</source>
        <translation>Buscar actualizaciones en línas cada vez que se inicie el programa</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2317"/>
        <source>Notification</source>
        <translation>Notificaciones</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2323"/>
        <source>Always show Result after Online Update Check</source>
        <translation>Mostrar siempre el resultado después de buscar actualizaciones</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2339"/>
        <source>Save</source>
        <translation>Guardar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2356"/>
        <source>Reset</source>
        <translation>Reestablecer</translation>
    </message>
    <message>
        <location filename="../DialogSettings.ui" line="2373"/>
        <source>Close</source>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="139"/>
        <source>Choose Database Location</source>
        <translation>Escoja una ubicación para la base de datos</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="175"/>
        <source>Could not display manual!

%1</source>
        <translation>¡No se pudo mostar el manual!

%1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="302"/>
        <source>SQL encryption can&apos;t be enabled without password and will be disabled!</source>
        <translation>¡La encriptación SQL no puede ser habilitada sin una contraseña y va a deshabilitarse!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="309"/>
        <source>Please enter valid values for additional information of user 1!</source>
        <translation>Por favor, introduzca valores válidos para la información adicional del usuario 1.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="316"/>
        <source>Please enter valid values for additional information of user 2!</source>
        <translation>Por favor, introduzca valores válidos para la información adicional del usuario 2.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="323"/>
        <source>Entered age doesn&apos;t match selected age group for user 1!</source>
        <translation>La edad introducida no coincide con el grupo de edad seleccionado para el usuario 1.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="330"/>
        <source>Entered age doesn&apos;t match selected age group for user 2!</source>
        <translation>La edad introducida no coincide con el grupo de edad seleccionado para el usuario 2.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="338"/>
        <source>Please enable symbols or lines for chart!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="345"/>
        <source>Please enter a valid e-mail address!</source>
        <translation>Por favor, introduzca una dirección de correo válida.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="352"/>
        <source>Please enter a e-mail subject!</source>
        <translation>Por favor, introduzca un asunto para el mensaje.</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="359"/>
        <source>E-Mail message must contain $CHART, $TABLE and/or $STATS!</source>
        <translation>¡El mensaje de correo electrónico debe contener $CHART, $TABLE y/o $STATS!</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="452"/>
        <source>User 1</source>
        <translation>Usuario 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="466"/>
        <source>User 2</source>
        <translation>Usuario 1</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="579"/>
        <source>Blood Pressure Report</source>
        <translation>Reporte de presión arterial</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="580"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Estimado Dr. Casas:

Le adjunto mis datos de presión arterial para ese mes.

Reciba un cordial saludo,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../DialogSettings.cpp" line="612"/>
        <source>Abort setup and discard all changes?</source>
        <translation>¿Desea cancelar la configuración y descartar todos los cambios?</translation>
    </message>
</context>
<context>
    <name>DialogUpdate</name>
    <message>
        <location filename="../DialogUpdate.ui" line="14"/>
        <source>Online Update</source>
        <translation>Actualización en línea</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="27"/>
        <source>Info</source>
        <translation>Información</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="80"/>
        <source>Available Version</source>
        <translation>Versión disponible</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="90"/>
        <source>Update File Size</source>
        <translation>Tamaño de la actualización</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="104"/>
        <source>Installed Version</source>
        <translation>Versión instalada</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="156"/>
        <source>Details</source>
        <translation>Detalles</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="178"/>
        <location filename="../DialogUpdate.ui" line="196"/>
        <source>Download</source>
        <translation>Descargar</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.ui" line="216"/>
        <source>Ignore</source>
        <translation>Ignorar</translation>
    </message>
    <message numerus="yes">
        <location filename="../DialogUpdate.cpp" line="40"/>
        <source>!!! SSL WARNING - READ CAREFULLY !!!

Network connection problem(s):

%1
Do you wish to continue anyway?</source>
        <translation>
            <numerusform>¡ADVERTENCIA DE SSL - LEER DETENIDAMENTE!

Problema de conexión de red:

%1
¿Desea continuar de todos modos?</numerusform>
            <numerusform>¡ADVERTENCIA DE SSL - LEER DETENIDAMENTE!

Problemas de conexión de red:

%1
¿Desea continuar de todos modos?</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Downloading update failed!

%1</source>
        <translation>¡La descarga de una actualización ha fallado!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="57"/>
        <source>Checking update failed!

%1</source>
        <translation>¡La comprobación de una actualización ha fallado!

%1</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="135"/>
        <source>Unexpected response from update server!</source>
        <translation>¡Respuesta inesperada del servidor de actualizaciones!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="162"/>
        <source>No new version found.</source>
        <translation>No se ha encontrado una nueva versión.</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="182"/>
        <source>Update doesn&apos;t have expected size!

%L1 : %L2

Retry download…</source>
        <translation>¡La actualización no tiene el tamaño esperado!

%L1 : %L2

Reintente la descarga...</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="186"/>
        <source>Update saved to %1.

Start new version now?</source>
        <translation>Actualización guardada en %1.

¿Desea iniciar la nueva versión?</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="199"/>
        <source>Could not start new version!</source>
        <translation>¡No se pudo iniciar la nueva versión!</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="206"/>
        <source>Could not save update to %1!

%2</source>
        <translation>¡No se pudo guardar la actualización en %1!

%2</translation>
    </message>
    <message>
        <location filename="../DialogUpdate.cpp" line="242"/>
        <source>Really abort download?</source>
        <translation>¿De verdad desea cancelar la descarga?</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../MainWindow.ui" line="20"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universal Blood Pressure Manager</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="143"/>
        <source>Chart View</source>
        <translation>Vista de gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="171"/>
        <source>Table View</source>
        <translation>Vista de tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="211"/>
        <source>Date</source>
        <translation>Fecha</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="220"/>
        <source>Time</source>
        <translation>Hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="229"/>
        <location filename="../MainWindow.cpp" line="77"/>
        <location filename="../MainWindow.cpp" line="3449"/>
        <source>Systolic</source>
        <translation>Sistólica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="238"/>
        <location filename="../MainWindow.cpp" line="78"/>
        <location filename="../MainWindow.cpp" line="3450"/>
        <source>Diastolic</source>
        <translation>Diastólica</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="247"/>
        <source>P.Pressure</source>
        <translation>Presión de pulso</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="256"/>
        <location filename="../MainWindow.cpp" line="79"/>
        <location filename="../MainWindow.cpp" line="3451"/>
        <source>Heartrate</source>
        <translation>Frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="265"/>
        <source>Irregular</source>
        <translation>Irregular</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="274"/>
        <source>Movement</source>
        <translation>Movimiento</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="283"/>
        <source>Invisible</source>
        <translation>Invisible</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="292"/>
        <source>Comment</source>
        <translation>Comentario</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="309"/>
        <source>Statistic View</source>
        <translation>Vista de estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="404"/>
        <source>¼ Hourly</source>
        <translation>15 minutos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="439"/>
        <source>½ Hourly</source>
        <translation>30 minutos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="471"/>
        <source>Hourly</source>
        <translation>Horaria</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="506"/>
        <source>¼ Daily</source>
        <translation>6 horas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="538"/>
        <source>½ Daily</source>
        <translation>12 horas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="570"/>
        <source>Daily</source>
        <translation>Diaria</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="605"/>
        <source>Weekly</source>
        <translation>Semanal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="637"/>
        <source>Monthly</source>
        <translation>Mensual</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="669"/>
        <source>Quarterly</source>
        <translation>Trimestral</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="701"/>
        <source>½ Yearly</source>
        <translation>Semestral</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="733"/>
        <source>Yearly</source>
        <translation>Anual</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="793"/>
        <source>Last 7 Days</source>
        <translation>Última semana</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="828"/>
        <source>Last 14 Days</source>
        <translation>Últimas 2 semanas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="860"/>
        <source>Last 21 Days</source>
        <translation>Últimas 3 semanas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="892"/>
        <source>Last 28 Days</source>
        <translation>Últimas 4 semanas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="924"/>
        <source>Last 3 Months</source>
        <translation>Últimos 3 meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="956"/>
        <source>Last 6 Months</source>
        <translation>Últimos 6 meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="988"/>
        <source>Last 9 Months</source>
        <translation>Últimos 9 meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1020"/>
        <source>Last 12 Months</source>
        <translation>Últimos 12 meses</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1052"/>
        <source>All Records</source>
        <translation>Todos los valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1099"/>
        <source>File</source>
        <translation>Archivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1103"/>
        <source>Print</source>
        <translation>Imprimir</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1125"/>
        <source>Help</source>
        <translation>Ayuda</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1129"/>
        <source>Make Donation</source>
        <translation>Hacer donación</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1132"/>
        <source>Donation</source>
        <translation>Donación</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1157"/>
        <source>Configuration</source>
        <translation>Configuración</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1161"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1170"/>
        <source>Language</source>
        <translation>Idioma</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1179"/>
        <source>Style</source>
        <translation>Estilo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1196"/>
        <source>Database</source>
        <translation>Base de datos</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1200"/>
        <source>Import</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1212"/>
        <source>Export</source>
        <translation>Exportar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1225"/>
        <source>Clear</source>
        <translation>Borrar valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1307"/>
        <source>Quit</source>
        <translation>Salir</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1310"/>
        <location filename="../MainWindow.ui" line="1313"/>
        <source>Quit Program</source>
        <translation>Salir del programa</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1325"/>
        <source>About</source>
        <translation>Acerca de...</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1328"/>
        <location filename="../MainWindow.ui" line="1331"/>
        <source>About Program</source>
        <translation>Acerca de este programa</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1340"/>
        <source>Guide</source>
        <translation>Guía</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1343"/>
        <location filename="../MainWindow.ui" line="1346"/>
        <source>Show Guide</source>
        <translation>Mostrar guía</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1355"/>
        <source>Update</source>
        <translation>Actualizar</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1358"/>
        <location filename="../MainWindow.ui" line="1361"/>
        <source>Check Update</source>
        <translation>Buscar actualizaciones</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1373"/>
        <source>Bugreport</source>
        <translation>Informe de errores</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1376"/>
        <location filename="../MainWindow.ui" line="1379"/>
        <source>Send Bugreport</source>
        <translation>Informar de un error</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1388"/>
        <source>Settings</source>
        <translation>Ajustes</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1391"/>
        <location filename="../MainWindow.ui" line="1394"/>
        <source>Change Settings</source>
        <translation>Modificar ajustes</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1409"/>
        <source>From Device</source>
        <translation>Desde un dispositivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1412"/>
        <location filename="../MainWindow.ui" line="1415"/>
        <source>Import From Device</source>
        <translation>Importar desde un dispositivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1427"/>
        <source>From File</source>
        <translation>Desde un archivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1430"/>
        <location filename="../MainWindow.ui" line="1433"/>
        <source>Import From File</source>
        <translation>Importar desde un archivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1442"/>
        <source>From Input</source>
        <translation>Manualmente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1445"/>
        <location filename="../MainWindow.ui" line="1448"/>
        <source>Import From Input</source>
        <translation>Importar manualmente</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1457"/>
        <source>To CSV</source>
        <translation>A CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1460"/>
        <location filename="../MainWindow.ui" line="1463"/>
        <source>Export To CSV</source>
        <translation>Exportar a CSV</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1475"/>
        <source>To XML</source>
        <translation>A XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1478"/>
        <location filename="../MainWindow.ui" line="1481"/>
        <source>Export To XML</source>
        <translation>Exportar a XML</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1490"/>
        <source>To JSON</source>
        <translation>A JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1493"/>
        <location filename="../MainWindow.ui" line="1496"/>
        <source>Export To JSON</source>
        <translation>Exportar a JSON</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1505"/>
        <source>To SQL</source>
        <translation>A SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1508"/>
        <location filename="../MainWindow.ui" line="1511"/>
        <source>Export To SQL</source>
        <translation>Exportar a SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1520"/>
        <source>Print Chart</source>
        <translation>Imprimir gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1523"/>
        <location filename="../MainWindow.ui" line="1526"/>
        <source>Print Chart View</source>
        <translation>Imprimir vista de gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1535"/>
        <source>Print Table</source>
        <translation>Imprimir tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1538"/>
        <location filename="../MainWindow.ui" line="1541"/>
        <source>Print Table View</source>
        <translation>Imprimir vista de tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1550"/>
        <source>Print Statistic</source>
        <translation>Imprimir estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1553"/>
        <location filename="../MainWindow.ui" line="1556"/>
        <source>Print Statistic View</source>
        <translation>Imprimir vista de estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1565"/>
        <source>Preview Chart</source>
        <translation>Previsualizar gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1568"/>
        <location filename="../MainWindow.ui" line="1571"/>
        <source>Preview Chart View</source>
        <translation>Previsualizar vista de gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1580"/>
        <source>Preview Table</source>
        <translation>Previsualizar tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1583"/>
        <location filename="../MainWindow.ui" line="1586"/>
        <source>Preview Table View</source>
        <translation>Previsualizar vista de tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1595"/>
        <source>Preview Statistic</source>
        <translation>Previsualizar estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1598"/>
        <location filename="../MainWindow.ui" line="1601"/>
        <source>Preview Statistic View</source>
        <translation>Previsualizar vista de estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1610"/>
        <location filename="../MainWindow.ui" line="1613"/>
        <location filename="../MainWindow.ui" line="1616"/>
        <source>Clear All</source>
        <translation>Borrar todos los valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1628"/>
        <location filename="../MainWindow.ui" line="1631"/>
        <location filename="../MainWindow.ui" line="1634"/>
        <source>Clear User 1</source>
        <translation>Borrar valores del usuario 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1643"/>
        <location filename="../MainWindow.ui" line="1646"/>
        <location filename="../MainWindow.ui" line="1649"/>
        <source>Clear User 2</source>
        <translation>Borrar valores del usuario 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1670"/>
        <location filename="../MainWindow.ui" line="1673"/>
        <location filename="../MainWindow.ui" line="1694"/>
        <location filename="../MainWindow.ui" line="1697"/>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="45"/>
        <location filename="../MainWindow.cpp" line="46"/>
        <location filename="../MainWindow.cpp" line="3261"/>
        <location filename="../MainWindow.cpp" line="3262"/>
        <location filename="../MainWindow.cpp" line="3263"/>
        <location filename="../MainWindow.cpp" line="3264"/>
        <location filename="../MainWindow.cpp" line="3483"/>
        <location filename="../MainWindow.cpp" line="3484"/>
        <location filename="../MainWindow.cpp" line="3485"/>
        <location filename="../MainWindow.cpp" line="3486"/>
        <source>Switch To %1</source>
        <translation>Cambiar a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1709"/>
        <source>Analysis</source>
        <translation>Análisis</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1712"/>
        <location filename="../MainWindow.ui" line="1715"/>
        <source>Analyze Records</source>
        <translation>Analizar registros</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1734"/>
        <location filename="../MainWindow.ui" line="1737"/>
        <location filename="../MainWindow.ui" line="1740"/>
        <source>Time Mode</source>
        <translation>Modo de tiempo</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1752"/>
        <source>PayPal</source>
        <translation>PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1755"/>
        <location filename="../MainWindow.ui" line="1758"/>
        <source>Donate via PayPal</source>
        <translation>Donar con PayPal</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1770"/>
        <location filename="../MainWindow.ui" line="1773"/>
        <source>Donate via Liberapay</source>
        <translation>Donar con Liberapay</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1785"/>
        <location filename="../MainWindow.ui" line="1788"/>
        <source>Donate via Amazon</source>
        <translation>Donar con Amazon</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1800"/>
        <location filename="../MainWindow.ui" line="1803"/>
        <source>Donate via SEPA</source>
        <translation>Donar con SEPA</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1812"/>
        <source>Translation</source>
        <translation>Traducción</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1815"/>
        <location filename="../MainWindow.ui" line="1818"/>
        <source>Contribute Translation</source>
        <translation>Contribuir traducción</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1827"/>
        <source>E-Mail</source>
        <translation>Correo electrónico</translation>
    </message>
    <message>
        <location filename="../MainWindow.ui" line="1830"/>
        <location filename="../MainWindow.ui" line="1833"/>
        <source>Send E-Mail</source>
        <translation>Enviar correo electrónico</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="43"/>
        <location filename="../MainWindow.cpp" line="44"/>
        <location filename="../MainWindow.cpp" line="251"/>
        <location filename="../MainWindow.cpp" line="3261"/>
        <location filename="../MainWindow.cpp" line="3262"/>
        <location filename="../MainWindow.cpp" line="3483"/>
        <location filename="../MainWindow.cpp" line="3484"/>
        <source>User 1</source>
        <translation>Usuario 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="45"/>
        <location filename="../MainWindow.cpp" line="46"/>
        <location filename="../MainWindow.cpp" line="261"/>
        <location filename="../MainWindow.cpp" line="3263"/>
        <location filename="../MainWindow.cpp" line="3264"/>
        <location filename="../MainWindow.cpp" line="3485"/>
        <location filename="../MainWindow.cpp" line="3486"/>
        <source>User 2</source>
        <translation>Usuario 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="49"/>
        <location filename="../MainWindow.cpp" line="50"/>
        <location filename="../MainWindow.cpp" line="3419"/>
        <location filename="../MainWindow.cpp" line="3420"/>
        <source>Records For Selected User</source>
        <translation>Registros para el usuario seleccionado</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="57"/>
        <location filename="../MainWindow.cpp" line="58"/>
        <location filename="../MainWindow.cpp" line="3422"/>
        <location filename="../MainWindow.cpp" line="3423"/>
        <source>Select Date &amp; Time</source>
        <translation>Seleccionar fecha y hora</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="87"/>
        <location filename="../MainWindow.cpp" line="3453"/>
        <source>Systolic - Value Range</source>
        <translation>Sistólica — intervalo de valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="88"/>
        <location filename="../MainWindow.cpp" line="3454"/>
        <source>Diastolic - Value Range</source>
        <translation>Diastólica — intervalo de valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="89"/>
        <location filename="../MainWindow.cpp" line="3455"/>
        <source>Heartrate - Value Range</source>
        <translation>Frecuencia cardíaca — intervalo de valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="91"/>
        <location filename="../MainWindow.cpp" line="3456"/>
        <source>Systolic - Target Area</source>
        <translation>Sistólica — área objetivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="92"/>
        <location filename="../MainWindow.cpp" line="3457"/>
        <source>Diastolic - Target Area</source>
        <translation>Diastólica — área objetivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="93"/>
        <location filename="../MainWindow.cpp" line="3458"/>
        <source>Heartrate - Target Area</source>
        <translation>Frecuencia cardíaca — área objetivo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="314"/>
        <source>Blood Pressure Report</source>
        <translation>Reporte de tensión arterial</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="315"/>
        <source>Dear Dr. House,

please find attached my blood pressure data for this month.

Best regards,
$USER
$CHART$TABLE$STATS</source>
        <translation>Estimado Dr. Casas:

Le adjunto mis datos de presión arterial para ese mes.

Reciba un cordial saludo,
$USER
$CHART$TABLE$STATS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="711"/>
        <source>Measurements : %1  |  Irregular : %2  |  Movement : %3</source>
        <translation>Registros : %1  |  Irregulares : %2  |  Movidos : %3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="712"/>
        <source>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</source>
        <translation>SYS : Ø %1 / x̃ %4  |  DIA : Ø %2 / x̃ %5  |  BPM : Ø %3 / x̃ %6</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="716"/>
        <source>Measurements : 0  |  Irregular : 0  |  Movement : 0</source>
        <translation>Registros : 0  |  Irregulares : 0  |  Movidos : 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="717"/>
        <source>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</source>
        <translation>SYS : Ø 0 / x̃ 0  |  DIA : Ø 0 / x̃ 0  |  BPM : Ø 0 / x̃ 0</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1176"/>
        <source>Athlete</source>
        <translation>Atleta</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1176"/>
        <source>To Low</source>
        <translation>Demasiado bajo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1177"/>
        <source>Excellent</source>
        <translation>Exelente</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1177"/>
        <source>Optimal</source>
        <translation>Óptimo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1178"/>
        <source>Great</source>
        <translation>Muy bueno</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1178"/>
        <source>Normal</source>
        <translation>Normal</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1179"/>
        <source>Good</source>
        <translation>Bueno</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1179"/>
        <source>High Normal</source>
        <translation>Normal–alto</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1180"/>
        <location filename="../MainWindow.cpp" line="1650"/>
        <location filename="../MainWindow.cpp" line="3462"/>
        <location filename="../MainWindow.cpp" line="3466"/>
        <location filename="../MainWindow.cpp" line="3470"/>
        <location filename="../MainWindow.h" line="294"/>
        <location filename="../MainWindow.h" line="298"/>
        <location filename="../MainWindow.h" line="302"/>
        <source>Average</source>
        <translation>Promedio</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1180"/>
        <source>Hyper 1</source>
        <translation>Híper 1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1181"/>
        <source>Below Average</source>
        <translation>Por debajo de la media</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1181"/>
        <source>Hyper 2</source>
        <translation>Híper 2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1182"/>
        <source>Poor</source>
        <translation>Pobre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1182"/>
        <source>Hyper 3</source>
        <translation>Híper 3</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1331"/>
        <source>Scanning import plugin &quot;%1&quot; failed!

%2</source>
        <translation>¡Ha ocurrido un error al escanear el complemento de importación «%1»!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1344"/>
        <location filename="../MainWindow.cpp" line="1366"/>
        <location filename="../MainWindow.cpp" line="3429"/>
        <source>Switch Language to %1</source>
        <translation>Cambiar idioma a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1391"/>
        <location filename="../MainWindow.cpp" line="1406"/>
        <location filename="../MainWindow.cpp" line="3437"/>
        <source>Switch Theme to %1</source>
        <translation>Cambiar tema a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1431"/>
        <location filename="../MainWindow.cpp" line="1451"/>
        <location filename="../MainWindow.cpp" line="3445"/>
        <source>Switch Style to %1</source>
        <translation>Cambiar estilo a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1519"/>
        <location filename="../MainWindow.cpp" line="1536"/>
        <location filename="../MainWindow.cpp" line="4148"/>
        <source>Delete record</source>
        <translation>Eliminar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1520"/>
        <location filename="../MainWindow.cpp" line="1543"/>
        <location filename="../MainWindow.cpp" line="4151"/>
        <source>Hide record</source>
        <translation>Ocultar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1522"/>
        <location filename="../MainWindow.cpp" line="1547"/>
        <source>Edit record</source>
        <translation>Editar registro</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1538"/>
        <location filename="../MainWindow.cpp" line="4162"/>
        <source>Really delete selected record?</source>
        <translation>¿De verdad desear eliminar el registro seleccionado?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1650"/>
        <location filename="../MainWindow.cpp" line="3463"/>
        <location filename="../MainWindow.cpp" line="3467"/>
        <location filename="../MainWindow.cpp" line="3471"/>
        <location filename="../MainWindow.h" line="295"/>
        <location filename="../MainWindow.h" line="299"/>
        <location filename="../MainWindow.h" line="303"/>
        <source>Median</source>
        <translation>Mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1652"/>
        <source>Click to swap Average and Median</source>
        <translation>Pulse para intercambiar media y mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1747"/>
        <source>Click to swap Legend and Label</source>
        <translation>Pulse para intercambiar leyenda y etiqueta</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1824"/>
        <source>No records to preview for selected time range!</source>
        <translation>¡No hay registros que previsualizar en el intervalo temporal seleccionado!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1856"/>
        <source>No records to print for selected time range!</source>
        <translation>¡No hay registros que imprimir en el intervalo temporal seleccionado!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1888"/>
        <location filename="../MainWindow.cpp" line="1938"/>
        <location filename="../MainWindow.cpp" line="2012"/>
        <location filename="../MainWindow.cpp" line="2130"/>
        <source>Created with UBPM for
Windows / Linux / MacOS</source>
        <translation>Creado con UBPM para Windows / Linux / macOS</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1889"/>
        <location filename="../MainWindow.cpp" line="1939"/>
        <location filename="../MainWindow.cpp" line="2013"/>
        <location filename="../MainWindow.cpp" line="2131"/>
        <source>Free and OpenSource
https://codeberg.org/lazyt/ubpm</source>
        <translation>Libre y de código abierto
https://codeberg.org/lazyt/ubpm</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1894"/>
        <location filename="../MainWindow.cpp" line="1944"/>
        <location filename="../MainWindow.cpp" line="2018"/>
        <location filename="../MainWindow.cpp" line="2136"/>
        <source>%1 (Age: %2, Height: %3, Weight: %4)</source>
        <translation>%1 (Edad: %2, Altura: %3, Peso: %4)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="1894"/>
        <location filename="../MainWindow.cpp" line="1944"/>
        <location filename="../MainWindow.cpp" line="2018"/>
        <location filename="../MainWindow.cpp" line="2136"/>
        <source>User %1</source>
        <translation>Usuario %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>DATE</source>
        <translation>FECHA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>TIME</source>
        <translation>HORA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>SYS</source>
        <translation>SISTÓLICA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>DIA</source>
        <translation>DIASTÓLICA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>PPR</source>
        <translation>PRESIÓN DE PULSO</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>BPM</source>
        <translation>FRECUENCIA CARDÍACA</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>IHB</source>
        <translation>PULSO IRREGULAR</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2052"/>
        <source>COMMENT</source>
        <translation>COMENTARIO</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2200"/>
        <source>Could not create e-mail because generating base64 for attachment &quot;%1&quot; failed!

%2</source>
        <translation>No se pudo crear el mensaje porque hubo un fallo al generar el código Base64 para el adjunto «%1»

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2263"/>
        <source>Import from CSV/XML/JSON/SQL</source>
        <translation>Importar desde CSV/XML/JSON/SQL</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2263"/>
        <source>CSV File (*.csv);;XML File (*.xml);;JSON File (*.json);;SQL File (*.sql)</source>
        <translation>Archivo CSV (*.csv);;Archivo XML (*.xml);;Archivo JSON (*.json);;Archivo SQL (*.sql)</translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2301"/>
        <location filename="../MainWindow.cpp" line="3078"/>
        <source>Successfully imported %n record(s) from %1.

     User 1 : %2
     User 2 : %3</source>
        <translation>
            <numerusform>¡Se ha importado %n registro exitosamente desde %1.

     Usuario 1 : %2
     Usuario 2 : %3</numerusform>
            <numerusform>¡Se han importado %n registros exitosamente desde %1.

     Usuario 1 : %2
     Usuario 2 : %3</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2305"/>
        <source>Skipped %n invalid record(s)!</source>
        <translation>
            <numerusform>¡%n registro inválido omitido!</numerusform>
            <numerusform>¡%n registros inválidos omitidos!</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="../MainWindow.cpp" line="2310"/>
        <location filename="../MainWindow.cpp" line="3082"/>
        <source>Skipped %n duplicate record(s)!</source>
        <translation>
            <numerusform>¡%n registro duplicado omitido!</numerusform>
            <numerusform>¡%n registros duplicados omitidos!</numerusform>
        </translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2326"/>
        <source>Could not open &quot;%1&quot;!

Reason: %2</source>
        <translation>¡No se pudo abrir «%1»!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2696"/>
        <source>Doesn&apos;t look like a UBPM database!

Maybe wrong encryption settings/password?</source>
        <translation>¡No parece que sea una base de datos de UBPM!

¿Puede que la contraseña o los ajustes de encriptación sean incorrectos?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2745"/>
        <source>Export to %1</source>
        <translation>Exportar a %1</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2745"/>
        <source>%1 File (*.%2)</source>
        <translation>Archivo %1 (*.%2)</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2776"/>
        <source>Could not create &quot;%1&quot;!

Reason: %2</source>
        <translation>¡No se pudo crear «%1»!

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2782"/>
        <source>The database is empty, no records to export!</source>
        <translation>La base de datos está vacía, ¡no hay registros que exportar!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2940"/>
        <source>Morning</source>
        <translation>Mañana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2940"/>
        <source>Afternoon</source>
        <translation>Tarde</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2948"/>
        <source>Week</source>
        <translation>Semana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2956"/>
        <source>Quarter</source>
        <translation>Trimestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2960"/>
        <source>Half Year</source>
        <translation>Semestre</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="2964"/>
        <source>Year</source>
        <translation>Año</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3012"/>
        <source>Really delete all records for user %1?</source>
        <translation>¿De verdad desea eliminar todos los registros del usuario %1?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3027"/>
        <source>All records for user %1 deleted and existing database saved to &quot;ubpm.sql.bak&quot;.</source>
        <translation>Todos los registros del usuario %1 han sido eliminados, y la base de datos ha sido guardada en «ubpm.sql.bak»</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3132"/>
        <source>Really delete all records?</source>
        <translation>¿De verdad desea eliminar todos los registros?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3143"/>
        <source>All records deleted and existing database &quot;ubpm.sql&quot; moved to trash.</source>
        <translation>Todos los registros han sido eliminados y la base de datos «ubpm.sql» ha sido movida a la papelera.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3186"/>
        <source>Chart</source>
        <translation>Gráfico</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3203"/>
        <source>Table</source>
        <translation>Tabla</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3220"/>
        <source>Statistic</source>
        <translation>Estadísticas</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3238"/>
        <source>Could not open e-mail &quot;%1&quot;!

%2</source>
        <translation>¡No se pudo abrir el correo &quot;%1&quot;!

%2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3248"/>
        <source>Could not start e-mail client!</source>
        <translation>¡No se pudo iniciar el cliente de correo!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3334"/>
        <source>Version</source>
        <translation>Versión</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3334"/>
        <source>Thanks to all translators:</source>
        <translation>Gracias a todos los traductores:</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3334"/>
        <source>This program may be installed and used free of charge for non-commercial use on as many computers as you like without limitations. A liability for any damages resulting from the use is excluded. Use at your own risk.</source>
        <translation>Este programa puede ser instalado y utilizado gratuitamente, para uso no comercial, en tantos ordenadores como desee y sin limitación alguna. Se excluye la responsabilidad por los daños resultantes del uso. Úselo bajo su propio riesgo.</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3354"/>
        <source>Please purchase a voucher in the desired amount through your Amazon account, select E-Mail to lazyt@mailbox.org as delivery method and specify &quot;UBPM&quot; as message.

Thank you very much!</source>
        <translation>Compre un cupón por el importe deseado a través de su cuenta de Amazon, utilice el correo electrónico lazyt@mailbox.org como método de envío y especifique &quot;UBPM&quot; como mensaje.

¡Muchísimas gracias!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3371"/>
        <source>Please send me an E-Mail request to lazyt@mailbox.org so I can provide you with my current bank account informations.

Thank you very much!</source>
        <translation>Envíeme una solicitud por correo electrónico a lazyt@mailbox.org para que pueda proporcionarle la información de mi cuenta bancaria.

¡Muchas gracias!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3396"/>
        <source>Loading application translation failed!</source>
        <translation>¡Ha ocurrido un error al cargar la traducción de la aplicación!</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3405"/>
        <source>Loading Qt base translation for &quot;%1&quot; failed!

Internal base translations (like &quot;Yes/No&quot;) are not available.

Don&apos;t show this warning again?</source>
        <translation>¡Ha ocurrido un error al cargar la traducción base de Qt para «%1»!

Traducciones base internas (como «Sí/No») no estarán disponibles.

¿No mostrar este mensaje de nuevo?</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3460"/>
        <location filename="../MainWindow.cpp" line="3464"/>
        <location filename="../MainWindow.cpp" line="3468"/>
        <location filename="../MainWindow.h" line="292"/>
        <location filename="../MainWindow.h" line="296"/>
        <location filename="../MainWindow.h" line="300"/>
        <source>Minimum</source>
        <translation>Mínimo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3461"/>
        <location filename="../MainWindow.cpp" line="3465"/>
        <location filename="../MainWindow.cpp" line="3469"/>
        <location filename="../MainWindow.h" line="293"/>
        <location filename="../MainWindow.h" line="297"/>
        <location filename="../MainWindow.h" line="301"/>
        <source>Maximum</source>
        <translation>Máximo</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="3519"/>
        <source>Could not open theme &quot;%1&quot; file!

Reason: %2</source>
        <translation>No se pudo abrir el archivo de tema «%1»

Motivo: %2</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4067"/>
        <source>Dynamic Scaling</source>
        <translation>Escalado dinámico</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4068"/>
        <source>Colored Stripes</source>
        <translation>Bandas de colores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4070"/>
        <source>Show Symbols</source>
        <translation>Mostrar símbolos</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4071"/>
        <source>Show Lines</source>
        <translation>Mostrar líneas</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4073"/>
        <source>Colored Symbols</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4075"/>
        <source>Show Heartrate</source>
        <translation>Mostrar frecuencia cardíaca</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4111"/>
        <location filename="../MainWindow.cpp" line="4122"/>
        <source>Symbols and lines can&apos;t be disabled both!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4150"/>
        <source>Show record</source>
        <translation>Mostrar toma</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4183"/>
        <source>Show Median</source>
        <translation>Mostrar mediana</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4184"/>
        <source>Show Values</source>
        <translation>Mostrar valores</translation>
    </message>
    <message>
        <location filename="../MainWindow.cpp" line="4225"/>
        <source>Really quit program?</source>
        <translation>¿Está seguro de que desea salir?</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../MainWindow.h" line="4"/>
        <source>Universal Blood Pressure Manager</source>
        <translation>Universal Blood Pressure Manager</translation>
    </message>
</context>
</TS>
