#include "DialogHelp.h"

DialogHelp::DialogHelp(QWidget *parent, QString page) : QDialog(parent)
{
	setupUi(this);

	setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint | Qt::WindowCloseButtonHint);

	language = reinterpret_cast<MainWindow*>(parent)->translatorApplication.language().isEmpty() ? "en" : reinterpret_cast<MainWindow*>(parent)->translatorApplication.language().mid(0, 2);

	if(!QFile::exists(QString("%1/Guides/%2.qhc").arg(QApplication::applicationDirPath(), language)))
	{
		if(!QFile::exists(QString("%1/Guides/en.qhc").arg(QApplication::applicationDirPath())))
		{
			page = "";
		}
		else
		{
			QMessageBox::information(parent, APPNAME, tr("%1 guide not found, showing EN guide instead.").arg(language.toUpper()));

			language ="en";
		}
	}

	QFile::copy(QString("%1/Guides/%2.qhc").arg(QApplication::applicationDirPath(), language), QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));
	QFile::copy(QString("%1/Guides/%2.qch").arg(QApplication::applicationDirPath(), language), QString("%1/%2.qch").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));

	if(!page.isEmpty())
	{
		helpEngine = new QHelpEngine(QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));
		helpEngine->setupData();

		contentWidget = helpEngine->contentWidget();
		contentWidget->setMinimumWidth(300);

		verticalLayout->addWidget((QWidget*)contentWidget);

		helpBrowser->setHelpEngine(helpEngine);

		splitter->setStretchFactor(0, 0);
		splitter->setStretchFactor(1, 1);

		connect(contentWidget, &QHelpContentWidget::clicked, this, &DialogHelp::setSourceFromContent);
		connect(contentWidget, &QHelpContentWidget::activated, this, &DialogHelp::setSourceFromContent);
		connect(helpBrowser, &HelpBrowser::anchorClicked, this, &DialogHelp::anchorClicked);
	}

	QTimer::singleShot(100, this, [this, page] { initAfterShown(page); });
}


void DialogHelp::initAfterShown(QString page)
{
	if(page.isEmpty())
	{
		QMessageBox::warning(this, APPNAME, tr("%1 guide not found!").arg(language.toUpper()));

		close();
	}
	else
	{
		contentWidget->expandAll();
		contentWidget->setFocus();

		setSourceFromPage(page);
	}
}

void DialogHelp::setSource(QUrl url)
{
	helpBrowser->setSource(url);

	contentWidget->setCurrentIndex(contentWidget->indexOf(url));
}
void DialogHelp::setSourceFromPage(QString page)
{
	setSource(QString("qthelp://de.lazyt.ubpm/%1/%2.html").arg(language, page));
}

void DialogHelp::setSourceFromContent(QModelIndex index)
{
	setSource(helpEngine->contentModel()->contentItemAt(index)->url());
}

void DialogHelp::anchorClicked(QUrl link)
{
	setSource(link);
}

DialogHelp::~DialogHelp()
{
	delete helpEngine;

	QFile::remove(QString("%1/%2.qch").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));
	QFile::remove(QString("%1/%2.qhc").arg(QStandardPaths::writableLocation(QStandardPaths::TempLocation), language));
}
