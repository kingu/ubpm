#include "DialogSettings.h"

DialogSettings::DialogSettings(QWidget *parent, struct SETTINGS *psettings, const QVector <QPluginLoader*> pplugins, int *pplugin) : QDialog(parent)
{
	int index = 0;

	setupUi(this);

	layout()->setSizeConstraint(QLayout::SetFixedSize);

	settings = psettings;
	plugins = pplugins;
	plugin = pplugin;

	plainTextEdit_message->setFixedHeight(3 * plainTextEdit_message->fontMetrics().height() + plainTextEdit_message->document()->documentMargin() + 5);

	lineEdit_location->setText(settings->database.location);
	groupBox_encryption->setEnabled(QSqlDatabase::isDriverAvailable("QSQLCIPHER"));
	groupBox_encryption->setChecked(settings->database.encryption);
	lineEdit_password->setText(settings->database.password);

	toolButton_female1->setChecked(settings->user[0].gender == "Female" ? true : false);
	toolButton_female2->setChecked(settings->user[1].gender == "Female" ? true : false);
	comboBox_age1->setCurrentIndex(settings->user[0].agegroup);
	comboBox_age2->setCurrentIndex(settings->user[1].agegroup);
	lineEdit_user1->setText(settings->user[0].name);
	lineEdit_user2->setText(settings->user[1].name);
	groupBox_user1->setChecked(settings->user[0].addition);
	groupBox_user2->setChecked(settings->user[1].addition);
	spinBox_birth1->setMaximum(QDate::currentDate().year());
	spinBox_birth2->setMaximum(QDate::currentDate().year());
	spinBox_birth1->setValue(settings->user[0].birth);
	spinBox_birth2->setValue(settings->user[1].birth);
	spinBox_height1->setValue(settings->user[0].height);
	spinBox_height2->setValue(settings->user[1].height);
	spinBox_weight1->setValue(settings->user[0].weight);
	spinBox_weight2->setValue(settings->user[1].weight);

	foreach(QPluginLoader *plugin, plugins)
	{
		deviceInterface = qobject_cast<DeviceInterface*>(plugin->instance());
		deviceInfo = deviceInterface->getDeviceInfo();
		QString fileName = QFileInfo(plugin->fileName()).fileName();

		comboBox_plugins->addItem(deviceInfo.icon.pixmap(QSize(24, 24)), fileName);

		if(fileName == settings->device.plugin)
		{
			index = comboBox_plugins->count() - 1;
		}

		plugin->unload();
	}

	comboBox_plugins->setCurrentIndex(index);

	checkBox_dynamic->setChecked(settings->chart.dynamic);
	checkBox_colored->setChecked(settings->chart.colored);
	groupBox_symbols->setChecked(settings->chart.symbols);
	toolButton_symbolcolor->setChecked(settings->chart.symbolcolor);
	horizontalSlider_symbolsize->setValue(settings->chart.symbolsize);
	groupBox_lines->setChecked(settings->chart.lines);
	horizontalSlider_linewidth->setValue(settings->chart.linewidth);
	checkBox_heartrate->setChecked(settings->chart.heartrate);
	checkBox_hrsheet->setChecked(settings->chart.hrsheet);
	groupBox_hrsheet->setEnabled(settings->chart.heartrate);
	spinBox_sys_max1->setValue(settings->chart.range[0].sys_max);
	spinBox_sys_min1->setValue(settings->chart.range[0].sys_min);
	spinBox_dia_max1->setValue(settings->chart.range[0].dia_max);
	spinBox_dia_min1->setValue(settings->chart.range[0].dia_min);
	spinBox_bpm_max1->setValue(settings->chart.range[0].bpm_max);
	spinBox_bpm_min1->setValue(settings->chart.range[0].bpm_min);
	spinBox_sys_max2->setValue(settings->chart.range[1].sys_max);
	spinBox_sys_min2->setValue(settings->chart.range[1].sys_min);
	spinBox_dia_max2->setValue(settings->chart.range[1].dia_max);
	spinBox_dia_min2->setValue(settings->chart.range[1].dia_min);
	spinBox_bpm_max2->setValue(settings->chart.range[1].bpm_max);
	spinBox_bpm_min2->setValue(settings->chart.range[1].bpm_min);

	horizontalSlider_sys1->setValue(settings->table[0].warnsys);
	horizontalSlider_dia1->setValue(settings->table[0].warndia);
	horizontalSlider_ppr1->setValue(settings->table[0].warnppr);
	horizontalSlider_bpm1->setValue(settings->table[0].warnbpm);
	horizontalSlider_sys2->setValue(settings->table[1].warnsys);
	horizontalSlider_dia2->setValue(settings->table[1].warndia);
	horizontalSlider_ppr2->setValue(settings->table[1].warnppr);
	horizontalSlider_bpm2->setValue(settings->table[1].warnbpm);

	checkBox_median->setChecked(settings->stats.median);
	checkBox_legend->setChecked(settings->stats.legend);

	lineEdit_address->setText(settings->email.address);
	lineEdit_subject->setText(settings->email.subject);
	plainTextEdit_message->setPlainText(settings->email.message);

	checkBox_autostart->setChecked(settings->update.autostart);
	checkBox_notification->setChecked(settings->update.notification);
}

void DialogSettings::on_checkBox_heartrate_toggled(bool checked)
{
	groupBox_hrsheet->setEnabled(checked);
}

void DialogSettings::on_checkBox_autostart_toggled(bool checked)
{
	groupBox_notification->setEnabled(checked);
}

void DialogSettings::on_comboBox_plugins_currentIndexChanged(int index)
{
	if(index)
	{
		deviceInterface = qobject_cast<DeviceInterface*>(plugins.at(index - 1)->instance());
		deviceInfo = deviceInterface->getDeviceInfo();

		toolButton_png->setEnabled(true);
		toolButton_pdf->setEnabled(true);

		label_producer->setText(deviceInfo.producer);
		label_model->setText(deviceInfo.alias.isEmpty() ? deviceInfo.model : deviceInfo.model + " (" + deviceInfo.alias + ")");
		label_version->setText(deviceInfo.version);
		label_maintainer->setText(deviceInfo.maintainer);
	}
	else
	{
		toolButton_png->setDisabled(true);
		toolButton_pdf->setDisabled(true);

		label_producer->clear();
		label_model->clear();
		label_version->clear();
		label_maintainer->clear();
	}
}

void DialogSettings::on_toolButton_choose_clicked()
{
	QString dir = QFileDialog::getExistingDirectory(this, tr("Choose Database Location"), QStandardPaths::writableLocation(QStandardPaths::DocumentsLocation), QFileDialog::ShowDirsOnly);

	if(!dir.isEmpty())
	{
		lineEdit_location->setText(dir + "/ubpm.sql");
	}
}

void DialogSettings::on_toolButton_view_pressed()
{
	lineEdit_password->setEchoMode(QLineEdit::QLineEdit::Normal);
}

void DialogSettings::on_toolButton_view_released()
{
	lineEdit_password->setEchoMode(QLineEdit::Password);
}

void DialogSettings::on_toolButton_png_clicked()
{
	QToolTip::showText(QCursor::pos(), QString("<img src='data:image/png;base64,%1>").arg(deviceInfo.image.toBase64().data()));
}

void DialogSettings::on_toolButton_pdf_clicked()
{
	QFile manual(MANUAL);

	if(manual.open(QIODevice::WriteOnly))
	{
		manual.write(deviceInfo.manual);
		manual.close();

		QDesktopServices::openUrl(QUrl::fromLocalFile(MANUAL));
	}
	else
	{
		QMessageBox::warning(this, APPNAME, tr("Could not display manual!\n\n%1").arg(manual.errorString()));
	}
}

void DialogSettings::on_horizontalSlider_symbolsize_valueChanged(int value)
{
	label_symbolsize->setNum(value);
}

void DialogSettings::on_horizontalSlider_linewidth_valueChanged(int value)
{
	label_linewidth->setNum(value);
}

void DialogSettings::on_horizontalSlider_sys1_valueChanged(int value)
{
	label_sys1->setNum(value);
}

void DialogSettings::on_horizontalSlider_dia1_valueChanged(int value)
{
	label_dia1->setNum(value);
}

void DialogSettings::on_horizontalSlider_ppr1_valueChanged(int value)
{
	label_ppr1->setNum(value);
}

void DialogSettings::on_horizontalSlider_bpm1_valueChanged(int value)
{
	label_bpm1->setNum(value);
}

void DialogSettings::on_horizontalSlider_sys2_valueChanged(int value)
{
	label_sys2->setNum(value);
}

void DialogSettings::on_horizontalSlider_dia2_valueChanged(int value)
{
	label_dia2->setNum(value);
}

void DialogSettings::on_horizontalSlider_ppr2_valueChanged(int value)
{
	label_ppr2->setNum(value);
}

void DialogSettings::on_horizontalSlider_bpm2_valueChanged(int value)
{
	label_bpm2->setNum(value);
}

bool DialogSettings::settingsUnchanged()
{
	if((settings->database.location != lineEdit_location->text()) || (settings->database.encryption != groupBox_encryption->isChecked()) || (settings->database.password != lineEdit_password->text()))
	{
		return false;
	}

	if((settings->user[0].gender != (toolButton_male1->isChecked() ? "Male" : "Female")) || (settings->user[0].agegroup != comboBox_age1->currentIndex()) || (settings->user[0].name != lineEdit_user1->text()) || (settings->user[0].addition != groupBox_user1->isChecked()) || (settings->user[0].birth != spinBox_birth1->value()) || (settings->user[0].height != spinBox_height1->value()) || (settings->user[0].weight != spinBox_weight1->value()))
	{
		return false;
	}

	if((settings->user[1].gender != (toolButton_male2->isChecked() ? "Male" : "Female")) || (settings->user[1].agegroup != comboBox_age2->currentIndex()) || (settings->user[1].name != lineEdit_user2->text()) || (settings->user[1].addition != groupBox_user2->isChecked()) || (settings->user[1].birth != spinBox_birth2->value()) || (settings->user[1].height != spinBox_height2->value()) || (settings->user[1].weight != spinBox_weight2->value()))
	{
		return false;
	}

	if(settings->device.plugin != (comboBox_plugins->currentIndex() ? comboBox_plugins->currentText() : ""))
	{
		return false;
	}

	if((settings->chart.dynamic != checkBox_dynamic->isChecked()) || (settings->chart.colored != checkBox_colored->isChecked()) || (settings->chart.symbols != groupBox_symbols->isChecked()) || (settings->chart.symbolcolor != toolButton_symbolcolor->isChecked()) || (settings->chart.symbolsize != horizontalSlider_symbolsize->value()) || (settings->chart.lines != groupBox_lines->isChecked()) || (settings->chart.linewidth != horizontalSlider_linewidth->value()) || (settings->chart.heartrate != checkBox_heartrate->isChecked()) || (settings->chart.hrsheet != checkBox_hrsheet->isChecked()))
	{
		return false;
	}

	if((settings->chart.range[0].sys_max != spinBox_sys_max1->value()) || (settings->chart.range[0].sys_min != spinBox_sys_min1->value()) || (settings->chart.range[0].dia_max != spinBox_dia_max1->value()) || (settings->chart.range[0].dia_min != spinBox_dia_min1->value()) || (settings->chart.range[0].bpm_max != spinBox_bpm_max1->value()) || (settings->chart.range[0].bpm_min != spinBox_bpm_min1->value()))
	{
		return false;
	}

	if((settings->chart.range[1].sys_max != spinBox_sys_max2->value()) || (settings->chart.range[1].sys_min != spinBox_sys_min2->value()) || (settings->chart.range[1].dia_max != spinBox_dia_max2->value()) || (settings->chart.range[1].dia_min != spinBox_dia_min2->value()) || (settings->chart.range[1].bpm_max != spinBox_bpm_max2->value()) || (settings->chart.range[1].bpm_min != spinBox_bpm_min2->value()))
	{
		return false;
	}

	if((settings->table[0].warnsys != horizontalSlider_sys1->value()) || (settings->table[0].warndia != horizontalSlider_dia1->value()) || (settings->table[0].warnppr != horizontalSlider_ppr1->value()) || (settings->table[0].warnbpm != horizontalSlider_bpm1->value()) || (settings->table[1].warnsys != horizontalSlider_sys2->value()) || (settings->table[1].warndia != horizontalSlider_dia2->value()) || (settings->table[1].warnppr != horizontalSlider_ppr2->value()) || (settings->table[1].warnbpm != horizontalSlider_bpm2->value()))
	{
		return false;
	}

	if((settings->stats.median != checkBox_median->isChecked()) || (settings->stats.legend != checkBox_legend->isChecked()))
	{
		return false;
	}

	if((settings->email.address != lineEdit_address->text()) || (settings->email.subject != lineEdit_subject->text()) || (settings->email.message != plainTextEdit_message->toPlainText()))
	{
		return false;
	}

	if((settings->update.autostart != checkBox_autostart->isChecked()) || (settings->update.notification != checkBox_notification->isChecked()))
	{
		return false;
	}

	return true;
}

void DialogSettings::on_pushButton_save_clicked()
{
	int age1 = QDate::currentDate().year() - spinBox_birth1->value();
	int age2 = QDate::currentDate().year() - spinBox_birth2->value();
	QStringList ages1 = comboBox_age1->currentText().split(' ');
	QStringList ages2 = comboBox_age2->currentText().split(' ');
	QRegularExpression mailaddr("\\b[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}\\b", QRegularExpression::CaseInsensitiveOption);

	if(ages1.count() < 3) ages1.append("9999");
	if(ages2.count() < 3) ages2.append("9999");

	if(groupBox_encryption->isChecked() && lineEdit_password->text().isEmpty())
	{
		QMessageBox::warning(this, APPNAME, tr("SQL encryption can't be enabled without password and will be disabled!"));

		groupBox_encryption->setChecked(false);
	}

	if(groupBox_user1->isChecked() && (!spinBox_birth1->value() || !spinBox_height1->value() || !spinBox_weight1->value()))
	{
		QMessageBox::critical(this, APPNAME, tr("Please enter valid values for additional information of user 1!"));

		return;
	}

	if(groupBox_user2->isChecked() && (!spinBox_birth2->value() || !spinBox_height2->value() || !spinBox_weight2->value()))
	{
		QMessageBox::critical(this, APPNAME, tr("Please enter valid values for additional information of user 2!"));

		return;
	}

	if(groupBox_user1->isChecked() && (age1 < ages1.at(0).toInt() || age1 > ages1.at(2).toInt()))
	{
		QMessageBox::critical(this, APPNAME, tr("Entered age doesn't match selected age group for user 1!"));

		return;
	}

	if(groupBox_user2->isChecked() && (age2 < ages2.at(0).toInt() || age2 > ages2.at(2).toInt()))
	{
		QMessageBox::critical(this, APPNAME, tr("Entered age doesn't match selected age group for user 2!"));

		return;
	}


	if(!groupBox_symbols->isChecked() && !groupBox_lines->isChecked())
	{
		QMessageBox::critical(this, APPNAME, tr("Please enable symbols or lines for chart!"));

		return;
	}

	if(!mailaddr.match(lineEdit_address->text()).hasMatch())
	{
		QMessageBox::critical(this, APPNAME, tr("Please enter a valid e-mail address!"));

		return;
	}

	if(lineEdit_subject->text().isEmpty())
	{
		QMessageBox::critical(this, APPNAME, tr("Please enter a e-mail subject!"));

		return;
	}

	if(!plainTextEdit_message->toPlainText().contains("$CHART") && !plainTextEdit_message->toPlainText().contains("$TABLE") && !plainTextEdit_message->toPlainText().contains("$STATS"))
	{
		QMessageBox::critical(this, APPNAME, tr("E-Mail message must contain $CHART, $TABLE and/or $STATS!"));

		return;
	}

	settings->database.location = lineEdit_location->text();
	settings->database.encryption = groupBox_encryption->isChecked();
	settings->database.password = lineEdit_password->text();

	settings->user[0].gender = toolButton_male1->isChecked() ? "Male" : "Female";
	settings->user[1].gender = toolButton_male2->isChecked() ? "Male" : "Female";
	settings->user[0].agegroup = comboBox_age1->currentIndex();
	settings->user[1].agegroup = comboBox_age2->currentIndex();
	settings->user[0].name = lineEdit_user1->text();
	settings->user[1].name = lineEdit_user2->text();
	settings->user[0].birth = spinBox_birth1->value();
	settings->user[1].birth = spinBox_birth2->value();
	settings->user[0].addition = groupBox_user1->isChecked();
	settings->user[1].addition = groupBox_user2->isChecked();
	settings->user[0].height = spinBox_height1->value();
	settings->user[1].height = spinBox_height2->value();
	settings->user[0].weight = spinBox_weight1->value();
	settings->user[1].weight = spinBox_weight2->value();

	settings->device.plugin = comboBox_plugins->currentIndex() ? comboBox_plugins->currentText() : "";
	*plugin = comboBox_plugins->currentIndex() - 1;

	settings->chart.dynamic = checkBox_dynamic->isChecked();
	settings->chart.colored = checkBox_colored->isChecked();
	settings->chart.symbols = groupBox_symbols->isChecked();
	settings->chart.symbolcolor = toolButton_symbolcolor->isChecked();
	settings->chart.symbolsize = horizontalSlider_symbolsize->value();
	settings->chart.lines = groupBox_lines->isChecked();
	settings->chart.linewidth = horizontalSlider_linewidth->value();
	settings->chart.heartrate = checkBox_heartrate->isChecked();
	settings->chart.hrsheet = checkBox_hrsheet->isChecked();
	settings->chart.range[0].sys_max = spinBox_sys_max1->value();
	settings->chart.range[0].sys_min = spinBox_sys_min1->value();
	settings->chart.range[0].dia_max = spinBox_dia_max1->value();
	settings->chart.range[0].dia_min = spinBox_dia_min1->value();
	settings->chart.range[0].bpm_max = spinBox_bpm_max1->value();
	settings->chart.range[0].bpm_min = spinBox_bpm_min1->value();
	settings->chart.range[1].sys_max = spinBox_sys_max2->value();
	settings->chart.range[1].sys_min = spinBox_sys_min2->value();
	settings->chart.range[1].dia_max = spinBox_dia_max2->value();
	settings->chart.range[1].dia_min = spinBox_dia_min2->value();
	settings->chart.range[1].bpm_max = spinBox_bpm_max2->value();
	settings->chart.range[1].bpm_min = spinBox_bpm_min2->value();

	settings->table[0].warnsys = horizontalSlider_sys1->value();
	settings->table[0].warndia = horizontalSlider_dia1->value();
	settings->table[0].warnppr = horizontalSlider_ppr1->value();
	settings->table[0].warnbpm = horizontalSlider_bpm1->value();
	settings->table[1].warnsys = horizontalSlider_sys2->value();
	settings->table[1].warndia = horizontalSlider_dia2->value();
	settings->table[1].warnppr = horizontalSlider_ppr2->value();
	settings->table[1].warnbpm = horizontalSlider_bpm2->value();

	settings->stats.median = checkBox_median->isChecked();
	settings->stats.legend = checkBox_legend->isChecked();

	settings->email.address = lineEdit_address->text();
	settings->email.subject = lineEdit_subject->text();
	settings->email.message = plainTextEdit_message->toPlainText();

	settings->update.autostart = checkBox_autostart->isChecked();
	settings->update.notification = checkBox_notification->isChecked();

	done(QDialog::Accepted);
}

void DialogSettings::on_pushButton_reset_clicked()
{
	switch(tabWidget->currentIndex())
	{
		case 0:
		{
			lineEdit_location->setText(FILE_DATABASE);

			groupBox_encryption->setChecked(false);
			lineEdit_password->clear();

			break;
		}

		case 1:
		{
			switch(tabWidget_user->currentIndex())
			{
				case 0:
				{
					toolButton_male1->setChecked(true);
					comboBox_age1->setCurrentIndex(3);
					lineEdit_user1->setText(tr("User 1"));

					groupBox_user1->setChecked(false);
					spinBox_birth1->setValue(0);
					spinBox_height1->setValue(0);
					spinBox_weight1->setValue(0);

					break;
				}

				case 1:
				{
					toolButton_female2->setChecked(true);
					comboBox_age2->setCurrentIndex(3);
					lineEdit_user2->setText(tr("User 2"));

					groupBox_user2->setChecked(false);
					spinBox_birth2->setValue(0);
					spinBox_height2->setValue(0);
					spinBox_weight2->setValue(0);

					break;
				}
			}

			break;
		}

		case 2:
		{
			comboBox_plugins->setCurrentIndex(0);

			break;
		}

		case 3:
		{
			switch(tabWidget_chart->currentIndex())
			{
				case 0:
				{
					checkBox_dynamic->setChecked(true);
					checkBox_colored->setChecked(true);
					groupBox_symbols->setChecked(true);
					toolButton_symbolcolor->setChecked(false);
					horizontalSlider_symbolsize->setValue(24);
					groupBox_lines->setChecked(true);
					horizontalSlider_linewidth->setValue(3);

					break;
				}

				case 1:
				{
					checkBox_heartrate->setChecked(true);
					checkBox_hrsheet->setChecked(false);

					break;
				}

				case 2:
				{
					spinBox_sys_max1->setValue(SYSMAX);
					spinBox_sys_min1->setValue(SYSMIN);
					spinBox_dia_max1->setValue(DIAMAX);
					spinBox_dia_min1->setValue(DIAMIN);
					spinBox_bpm_max1->setValue(BPMMAX);
					spinBox_bpm_min1->setValue(BPMMIN);

					break;
				}

				case 3:
				{
					spinBox_sys_max2->setValue(SYSMAX);
					spinBox_sys_min2->setValue(SYSMIN);
					spinBox_dia_max2->setValue(DIAMAX);
					spinBox_dia_min2->setValue(DIAMIN);
					spinBox_bpm_max2->setValue(BPMMAX);
					spinBox_bpm_min2->setValue(BPMMIN);

					break;
				}
			}

			break;
		}

		case 4:
		{
			switch(tabWidget_table->currentIndex())
			{
				case 0:
				{
					horizontalSlider_sys1->setValue(WRNSYS);
					horizontalSlider_dia1->setValue(WRNDIA);
					horizontalSlider_ppr1->setValue(WRNPPR);
					horizontalSlider_bpm1->setValue(WRNBPM);

					break;
				}

				case 1:
				{
					horizontalSlider_sys2->setValue(WRNSYS);
					horizontalSlider_dia2->setValue(WRNDIA);
					horizontalSlider_ppr2->setValue(WRNPPR);
					horizontalSlider_bpm2->setValue(WRNBPM);

					break;
				}
			}

			break;
		}

		case 5:
		{
			checkBox_median->setChecked(true);
			checkBox_legend->setChecked(true);

			break;
		}

		case 6:
		{
			lineEdit_address->setText("e-m@il.net");
			lineEdit_subject->setText(tr("Blood Pressure Report"));
			plainTextEdit_message->setPlainText(tr("Dear Dr. House,\n\nplease find attached my blood pressure data for this month.\n\nBest regards,\n$USER\n$CHART$TABLE$STATS"));

			break;
		}

		case 7:
		{
			checkBox_autostart->setChecked(true);
			checkBox_notification->setChecked(true);

			break;
		}
	}
}

void DialogSettings::on_pushButton_close_clicked()
{
	close();
}

void DialogSettings::keyPressEvent(QKeyEvent *ke)
{
	if(ke->key() == Qt::Key_F1)
	{
		DialogHelp(reinterpret_cast<QWidget*>(parent()), QString("01-02-%1").arg(tabWidget->currentIndex() + 1, 2, 10, QChar('0'))).exec();
	}

	QDialog::keyPressEvent(ke);
}

void DialogSettings::reject()
{
	if(settingsUnchanged() || QMessageBox::question(this, APPNAME, tr("Abort setup and discard all changes?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		done(QDialog::Rejected);
	}
}
