<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../DialogImport.ui" line="14"/>
        <source>Device Import</source>
        <translation>Geräte Import</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="24"/>
        <source>Device Information</source>
        <translation>Geräte Information</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="53"/>
        <source>Serial</source>
        <translation>Seriennummer</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="60"/>
        <source>Producer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="67"/>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="145"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="162"/>
        <source>Write Logfile</source>
        <translation>Logdatei schreiben</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="188"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="18"/>
        <source>Loading plugin translation failed!</source>
        <translation>Laden der Erweiterung Übersetzung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="155"/>
        <source>Device doesn&apos;t respond, try again?</source>
        <translation>Gerät antwortet nicht, erneut versuchen?</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="251"/>
        <source>Could not open logfile %1!

%2</source>
        <translation>Konnte Logdatei %1 nicht öffnen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="362"/>
        <location filename="../../DialogImport.cpp" line="401"/>
        <source>Import aborted by user!</source>
        <translation>Import durch Benutzer abgebrochen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="42"/>
        <source>Could not open usb device %1:%2!</source>
        <translation>Konnte USB-Gerät %1:%2 nicht öffnen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="258"/>
        <source>Can&apos;t set device into info mode!</source>
        <translation>Gerät kann nicht in den Info-Modus gesetzt werden!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="271"/>
        <location filename="../../DialogImport.cpp" line="285"/>
        <location filename="../../DialogImport.cpp" line="310"/>
        <location filename="../../DialogImport.cpp" line="324"/>
        <location filename="../../DialogImport.cpp" line="341"/>
        <location filename="../../DialogImport.cpp" line="375"/>
        <location filename="../../DialogImport.cpp" line="414"/>
        <source>Send %1 command failed!</source>
        <translation>Sende Befehl %1 fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="297"/>
        <source>Can&apos;t set device into data mode!</source>
        <translation>Gerät kann nicht in den Daten-Modus gesetzt werden!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="454"/>
        <source>Import in progress!</source>
        <translation>Import wird durchgeführt!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="443"/>
        <source>Really abort import?</source>
        <translation>Import wirklich abbrechen?</translation>
    </message>
</context>
</TS>
