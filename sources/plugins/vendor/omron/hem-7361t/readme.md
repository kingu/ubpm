## Prerequisites

This plugin is "work in progress" and has the following limitations at the moment:

* bluetooth controller with changeable mac address is required (e.g. CSR8510 chipset, buy usb dongle for 5€)
* install the [OMRON Connect](https://www.omronconnect.com) app on your Android/Apple and add your device (skip all personal data, no cloud account)
* clone the bluetooth mac address of your Android/Apple to the usb dongle (bdaddr on Linux/MacOS or [MacAddressChanger](https://macaddresschanger.com) on Windows)
* connect the device via operating system to the usb dongle (Windows can only discover connected devices)

Select the bluetooth controller with cloned mac in UBPM and click "Discover device". If your device is missing try to discover again (scans only for 5 seconds).

After your device was found click "Connect device" and some informations like producer, product and firmware should be displayed.

Now click "Import" and keep your fingers crossed...

## Troubleshooting

Doesn't work? Here are some hints:

* disable the internal bluetooth controller in BIOS to force use of the external usb dongle
* discover again if the connection fails to avoid cached informations
* delete the old connection via operating systen and recreate again
* resync data once in the "OMRON Connect" app and repeat the previous step

## Cleanup

Maybe old data needs to be cleared to start from scratch. Try the following:

#### Windows

* delete HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\BTHPORT\Parameters\Devices\\\<mac>

#### Linux

* delete /var/lib/bluetooth/\<mac>

#### MacOS

* delete /Library/Preferences/com.apple.Bluetooth.plist
