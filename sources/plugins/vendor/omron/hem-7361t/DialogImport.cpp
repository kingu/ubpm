#include "DialogImport.h"

DialogImport::DialogImport(QWidget *parent, QString language, QString theme, QVector <struct HEALTHDATA> *user1, QVector <struct HEALTHDATA> *user2) : QDialog(parent)
{
	u1 = user1;
	u2 = user2;
	int controllers;

	setStyleSheet(theme);

	if(language != "en_US")
	{
		if(translatorPlugin.load(QString(":/qm/%1-%2.qm").arg(QString(MODEL).toLower(), language)))
		{
			qApp->installTranslator(&translatorPlugin);
		}
		else
		{
			QMessageBox::warning(nullptr, MODEL, tr("Could not load your selected language."));
		}
	}

	setupUi(this);

	setWindowTitle(MODEL);

	progressBar_1->setFormat(QString("0/%1 [%p%]").arg(MEMORY));
	progressBar_2->setFormat(QString("0/%1 [%p%]").arg(MEMORY));

	log.setFileName(LOGFILE);

	if(!(controllers = searchBtController()))
	{
		failed = true;
	}
	else if(controllers == 1)
	{
//		QTimer::singleShot(1, this, &DialogImport::on_pushButton_discover_clicked);
	}
}

int DialogImport::searchBtController()
{
	QList <QBluetoothHostInfo> bhil = QBluetoothLocalDevice::allDevices();

	if(bhil.count())
	{
		foreach(QBluetoothHostInfo bhi, bhil)
		{
			comboBox_controller->addItem(QString("%1 | %2").arg(bhi.address().toString(), bhi.name()), bhi.address().toString());
		}
	}
	else
	{
		QMessageBox::warning(nullptr, MODEL, tr("No Bluetooth controller found."));
	}

	return bhil.count();
}

// ###  QBluetoothDeviceDiscoveryAgent ###

void DialogImport::bddaDeviceDiscovered(QBluetoothDeviceInfo info)
{
	QStringList items;
	QString address;

#ifdef Q_OS_MACOS
	address = info.deviceUuid().toString();
#else
	address = info.address().toString();
#endif

	for(int i = 0; i < comboBox_device->count(); i++)
	{
		items.append(comboBox_device->itemData(i).toString());
	}

	if(!info.name().isEmpty() && !items.contains(address))
	{
		comboBox_device->addItem(info.name(), address);

		bdi.append(info);

		if(info.name() == "X4 Smart" || info.name() == "X7 Smart" || info.name() == "M4 Intelli IT" || info.name() == "M7 Intelli IT" || info.name() == "M400 Intelli IT" || info.name() == "M500 Intelli IT" || info.name().startsWith("BLESmart_"))
		{
			comboBox_device->setCurrentIndex(comboBox_device->count() - 1);

/*			bdda->stop();

			on_pushButton_connect_clicked();*/
		}
	}
}

void DialogImport::bddaError(QBluetoothDeviceDiscoveryAgent::Error /*error*/)
{
	comboBox_controller->setEnabled(true);
	pushButton_discover->setEnabled(true);

	QMessageBox::warning(this, MODEL, tr("Bluetooth error.\n\n%1").arg(bdda->errorString()));
}

void DialogImport::bddaFinished()
{
	comboBox_controller->setEnabled(true);
	pushButton_discover->setEnabled(true);

	if(comboBox_device->count())
	{
		comboBox_device->setEnabled(true);
		pushButton_connect->setEnabled(true);
	}
	else
	{
		QMessageBox::warning(this, MODEL, tr("No device discovered.\n\nCheck that the operating system wants to connect or press the Bluetooth button on the device and try again…"));
	}
}

// ### QLowEnergyController ###

void DialogImport::lecConnected()
{
	lec->discoverServices();
}

void DialogImport::lecDisconnected()
{
	comboBox_controller->setEnabled(true);
	pushButton_discover->setEnabled(true);

	comboBox_device->setEnabled(true);
	pushButton_connect->setEnabled(true);

	pushButton_import->setDisabled(true);
}

void DialogImport::lecDiscoveryFinished()
{
	if(!lec->services().contains(QBluetoothUuid(UUID_300)))
	{
		comboBox_controller->setEnabled(true);
		pushButton_discover->setEnabled(true);

		comboBox_device->setEnabled(true);
		pushButton_connect->setEnabled(true);

		QMessageBox::warning(this, MODEL, tr("The selected device is not a %1.").arg(MODEL));

		return;
	}

	readBTInfo();

	pushButton_import->setEnabled(true);
}

void DialogImport::lecError(QLowEnergyController::Error /*error*/)
{
	comboBox_controller->setEnabled(true);
	pushButton_discover->setEnabled(true);

	comboBox_device->setEnabled(true);
	pushButton_connect->setEnabled(true);

	QMessageBox::warning(this, MODEL, tr("Could not connect to the device.\n\nCheck that the operating system wants to connect or press the Bluetooth button on the device and try again…\n\n%1").arg(lec->errorString()));
}

// ### QLowEnergyService ###

void DialogImport::lesCharacteristicChanged(QLowEnergyCharacteristic characteristic, QByteArray value)
{
	if(les_data->state() != QLowEnergyService::DiscoveringServices)
	{
		static QByteArray rawdata;
		static int bytes = 0;

		if(characteristic.uuid() == UUID_311)
		{
			bt_finished = true;

			return;
		}
		else if(characteristic.uuid() == UUID_361)
		{
			bytes = quint8(value[0]);

			rawdata.clear();
			rawdata.append(value);
		}
		else
		{
			rawdata.append(value);
		}

		if(rawdata.size() >= bytes)
		{
			logRawData(false, characteristic, rawdata);

			if(quint8(rawdata[1]) == 0x81 && bytes > 8)
			{
				payloads[payload].append(rawdata.mid(6, 8));
			}

			bt_finished = true;
		}
	}
}

void DialogImport::lesCharacteristicRead(QLowEnergyCharacteristic characteristic, QByteArray value)
{
	if(characteristic.name() == "Manufacturer Name String")
	{
		label_producer->setText(value);
	}
	else if(characteristic.name() == "Model Number String")
	{
		label_product->setText(value);
	}
	else if(characteristic.name() == "Firmware Revision String")
	{
		label_firmware->setText(value);
	}
}

void DialogImport::lesCharacteristicWritten(QLowEnergyCharacteristic characteristic, QByteArray value)
{
	logRawData(true, characteristic, value);
}

bool DialogImport::waitBTFinished()
{
	QElapsedTimer timeout;

	bt_finished = false;

	timeout.start();

	while(!bt_finished)
	{
		QApplication::processEvents();

		QThread::usleep(250);

		if(timeout.hasExpired(5000))
		{
			QMessageBox::warning(this, MODEL, tr("No answer from the device.\n\nDid you use the Bluetooth controller you cloned the MAC address for?"));

			return false;
		}
	}

	return true;
}

void DialogImport::readBTInfo()
{
	if((les_info = lec->createServiceObject(QBluetoothUuid::DeviceInformation)))
	{
		les_info->discoverDetails();

		while(les_info->state() != QLowEnergyService::ServiceDiscovered)
		{
			QApplication::processEvents();
		}

		connect(les_info, &QLowEnergyService::characteristicRead, this, &DialogImport::lesCharacteristicRead);

		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::ModelNumberString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::ManufacturerNameString));
		les_info->readCharacteristic(les_info->characteristic(QBluetoothUuid::FirmwareRevisionString));
	}
	else
	{
		label_producer->setText("?");
		label_product->setText("?");
		label_firmware->setText("?");
	}
}

bool DialogImport::readBTData()
{
	if((les_data = lec->createServiceObject(QBluetoothUuid(UUID_300))))
	{
		int addr = 0x0098;																				// data start address
		QByteArray cmd_data = QByteArray::fromHex("0801000098100081");									// 08010000982000B1 or 08010000983000A1 for >16 bytes notifications
		QLowEnergyCharacteristic lechar311, lechar321, lechar361, lechar371/*, lechar381, lechar391*/;	// 381 + 391 only required for >16 bytes notifications
		QLowEnergyDescriptor ledesc311, ledesc321, ledesc361, ledesc371/*, ledesc381, ledesc391*/;

		les_data->discoverDetails();

		while(les_data->state() != QLowEnergyService::ServiceDiscovered)
		{
			QApplication::processEvents();
		}

		connect(les_data, &QLowEnergyService::characteristicChanged, this, &DialogImport::lesCharacteristicChanged);
		connect(les_data, &QLowEnergyService::characteristicWritten, this, &DialogImport::lesCharacteristicWritten);

		lechar311 = les_data->characteristic(QBluetoothUuid(UUID_311));
		lechar321 = les_data->characteristic(QBluetoothUuid(UUID_321));
		lechar361 = les_data->characteristic(QBluetoothUuid(UUID_361));
		lechar371 = les_data->characteristic(QBluetoothUuid(UUID_371));
/*		lechar381 = les_data->characteristic(QBluetoothUuid(UUID_381));
		lechar391 = les_data->characteristic(QBluetoothUuid(UUID_391));*/

		ledesc311 = lechar311.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
//		ledesc321 = lechar321.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
		ledesc361 = lechar361.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
		ledesc371 = lechar371.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
/*		ledesc381 = lechar381.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);
		ledesc391 = lechar391.descriptor(QBluetoothUuid::ClientCharacteristicConfiguration);*/

		les_data->writeDescriptor(ledesc311, QByteArray::fromHex("0100"));
		les_data->writeDescriptor(ledesc361, QByteArray::fromHex("0100"));
		les_data->writeDescriptor(ledesc371, QByteArray::fromHex("0100"));
/*		les_data->writeDescriptor(ledesc381, QByteArray::fromHex("0100"));
		les_data->writeDescriptor(ledesc391, QByteArray::fromHex("0100"));*/

		les_data->writeCharacteristic(lechar311, QByteArray::fromHex("0100000000000000000000000000000000"));
		waitBTFinished();

		les_data->writeCharacteristic(lechar321, cmd_init);
		if(!waitBTFinished()) return false;

		for(int i = 0; i < MEMORY; i++)
		{
			if(abort)
			{
				les_data->writeCharacteristic(lechar321, cmd_fail);

				QMessageBox::warning(this, MODEL, tr("Import aborted by user!"));

				done(QDialog::Rejected);

				return false;
			}

			bt_finished = false;

			les_data->writeCharacteristic(lechar321, cmd_data);

			if(!waitBTFinished())
			{
				return false;
			}

			if(!payload)
			{
				progressBar_1->setFormat(QString("%1/%2 [%p%]").arg(i+1).arg(MEMORY));
				progressBar_1->setValue((i+1)*100 / MEMORY);
			}
			else
			{
				progressBar_2->setFormat(QString("%1/%2 [%p%]").arg(i+1).arg(MEMORY));
				progressBar_2->setValue((i+1)*100 / MEMORY);
			}

			addr += 0x10;

			if(!payload && i == MEMORY-1)
			{
				i = 0;

				payload = 1;
			}

			cmd_data[3] = addr >> 8;
			cmd_data[4] = addr & 0xFF;
			cmd_data[7] = buildCRC(cmd_data);
		}

		les_data->writeCharacteristic(lechar321, cmd_done);
		if(!waitBTFinished()) return false;
	}
	else
	{
		QMessageBox::warning(this, MODEL, tr("Could not access the Bluetooth service %1.").arg(UUID_300.toString()));

		return false;
	}

	return true;
}

int DialogImport::buildCRC(QByteArray data)
{
	int crc = 0;
	int len = quint8(data[0]);

	while(--len)
	{
		crc ^= quint8(data[len - 1]);
	}

	return crc;
}

void DialogImport::decryptPayload()
{
	HEALTHDATA record;
	int user1 = payloads[0].size() / 8;
	int user2 = payloads[1].size() / 8;

	if(log.isOpen())
	{
		log.write(QString("\nUser 1: %1 Records, Payload = ").arg(user1, 3, 10, QChar('0')).toUtf8());
		log.write(payloads[0].toHex());

		log.write(QString("\nUser 2: %2 Records, Payload = ").arg(user2, 3, 10, QChar('0')).toUtf8());
		log.write(payloads[1].toHex());
	}

	for(int i = 0; i < 8*user1; i += 8)
	{
		record.dts = QDateTime(QDate(2000 + quint8(payloads[0][i + 3]), (quint8(payloads[0][i + 5])>>2) & 0x0F, ((quint8(payloads[0][i + 5])<<8 | quint8(payloads[0][i + 4]))>>5) & 0x1F), QTime(quint8(payloads[0][i + 4]) & 0x1F, ((quint8(payloads[0][i + 7])<<8 | quint8(payloads[0][i + 6]))>>6) & 0x3F, quint8(payloads[0][i + 6]) & 0x3F)).toMSecsSinceEpoch();
		record.sys = quint8(payloads[0][i]) + 25;
		record.dia = quint8(payloads[0][i + 1]);
		record.bpm = quint8(payloads[0][i + 2]);
		record.ihb = (quint8(payloads[0][i + 5])>>6) & 0x01;
		record.mov = quint8(payloads[0][i + 5])>>7;
		record.inv = false;
		record.msg = "";

		u1->append(record);
	}

	for(int i = 0; i < 8*user2; i += 8)
	{
		record.dts = QDateTime(QDate(2000 + quint8(payloads[1][i + 3]), (quint8(payloads[1][i + 5])>>2) & 0x0F, ((quint8(payloads[1][i + 5])<<8 | quint8(payloads[1][i + 4]))>>5) & 0x1F), QTime(quint8(payloads[1][i + 4]) & 0x1F, ((quint8(payloads[1][i + 7])<<8 | quint8(payloads[1][i + 6]))>>6) & 0x3F, quint8(payloads[1][i + 6]) & 0x3F)).toMSecsSinceEpoch();
		record.sys = quint8(payloads[1][i]) + 25;
		record.dia = quint8(payloads[1][i + 1]);
		record.bpm = quint8(payloads[1][i + 2]);
		record.ihb = (quint8(payloads[1][i + 5])>>6) & 0x01;
		record.mov = quint8(payloads[1][i + 5])>>7;
		record.inv = false;
		record.msg = "";

		u2->append(record);
	}
}

void DialogImport::logRawData(bool direction, QLowEnergyCharacteristic characteristics, QByteArray data)
{
	if(log.isOpen())
	{
		if(characteristics.uuid() != UUID_311)
		{
			log.write(QString("%1 %2 : %3 ").arg(direction ? "->" : "<-").arg(quint8(data[0]), 2, 10, QChar('0')).arg(characteristics.uuid().toString()).toUtf8());
			log.write(data.toHex(), quint8(data[0]) * 2);
			log.write("\n");
		}
	}
}

void DialogImport::on_comboBox_controller_currentIndexChanged(int /*index*/)
{
	comboBox_device->clear();
	bdi.clear();

	pushButton_connect->setDisabled(true);
}

void DialogImport::on_pushButton_discover_clicked()
{
	bdda = new QBluetoothDeviceDiscoveryAgent(QBluetoothAddress(comboBox_controller->currentData().toString()));
	bdda->setLowEnergyDiscoveryTimeout(5*1000);

	connect(bdda, &QBluetoothDeviceDiscoveryAgent::deviceDiscovered, this, &DialogImport::bddaDeviceDiscovered);
	connect(bdda, QOverload<QBluetoothDeviceDiscoveryAgent::Error>::of(&QBluetoothDeviceDiscoveryAgent::error), this, &DialogImport::bddaError);
	connect(bdda, &QBluetoothDeviceDiscoveryAgent::finished, this, &DialogImport::bddaFinished);

	comboBox_device->clear();
	bdi.clear();

	comboBox_controller->setDisabled(true);
	pushButton_discover->setDisabled(true);

	comboBox_device->setDisabled(true);
	pushButton_connect->setDisabled(true);

	bdda->start(QBluetoothDeviceDiscoveryAgent::LowEnergyMethod);	// only paired devices in Windows!
}

void DialogImport::on_pushButton_connect_clicked()
{
	bld = new QBluetoothLocalDevice(QBluetoothAddress(comboBox_controller->currentData().toString()));

	if(!bld->isValid())
	{
		QMessageBox::warning(this, MODEL, tr("The selected Bluetooth controller is not available."));

		return;
	}

#ifdef Q_OS_MACOS
	lec = QLowEnergyController::createCentral(bdi.at(comboBox_device->currentIndex()));
#else
	lec = QLowEnergyController::createCentral(QBluetoothAddress(bdi.at(comboBox_device->currentIndex()).address()), bld->address());
#endif

	connect(lec, &QLowEnergyController::connected, this, &DialogImport::lecConnected);
	connect(lec, &QLowEnergyController::disconnected, this, &DialogImport::lecDisconnected);
	connect(lec, &QLowEnergyController::discoveryFinished, this, &DialogImport::lecDiscoveryFinished);
	connect(lec, QOverload<QLowEnergyController::Error>::of(&QLowEnergyController::error), this, &DialogImport::lecError);

	comboBox_controller->setDisabled(true);
	pushButton_discover->setDisabled(true);

	comboBox_device->setDisabled(true);
	pushButton_connect->setDisabled(true);

	label_producer->clear();
	label_product->clear();
	label_firmware->clear();

	lec->connectToDevice();
}

void DialogImport::on_pushButton_import_clicked()
{
	pushButton_import->setDisabled(true);
	pushButton_cancel->setEnabled(true);

	if(toolButton->isChecked())
	{
		if(!log.isOpen())
		{
			if(log.open(QIODevice::WriteOnly))
			{
				log.write(QString("%1\n\n   Producer : %2\n   Product  : %3\n   Firmware : %4\n\n").arg(MODEL, label_producer->text(), label_product->text(), label_firmware->text()).toUtf8());
			}
			else
			{
				QMessageBox::critical(this, MODEL, tr("Could not open the logfile %1.\n\n%2").arg(log.fileName(), log.errorString()));
			}
		}
	}

	finished = false;

	if(!readBTData())
	{
		done(QDialog::Rejected);

		return;
	}

	finished = true;

	decryptPayload();

	done(QDialog::Accepted);
}

void DialogImport::on_pushButton_cancel_clicked()
{
	if(QMessageBox::question(this, MODEL, tr("Cancel import?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No) == QMessageBox::Yes)
	{
		finished = true;
		abort = true;
	}
}

void DialogImport::reject()
{
	if(!finished)
	{
		QMessageBox::warning(this, MODEL, tr("Import in progress…"));

		return;
	}

	log.close();

	QDialog::reject();
}
