<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE">
<context>
    <name>DialogImport</name>
    <message>
        <location filename="../../DialogImport.ui" line="20"/>
        <source>Device Import</source>
        <translation>Geräte Import</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="30"/>
        <source>Bluetooth Controller</source>
        <translation>Bluetooth Kontroller</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="36"/>
        <source>Select Bluetooth Controller</source>
        <translation>Bluetooth-Kontroller auswählen</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="43"/>
        <source>Discover Device</source>
        <translation>Gerät entdecken</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="57"/>
        <source>Bluetooth Device</source>
        <translation>Bluetooth Gerät</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="66"/>
        <source>Select Bluetooth Device</source>
        <translation>Bluetooth-Gerät auswählen</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="76"/>
        <source>Connect Device</source>
        <translation>Gerät verbinden</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="90"/>
        <source>Device Information</source>
        <translation>Geräte Information</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="119"/>
        <source>Firmware</source>
        <translation>Firmware</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="126"/>
        <source>Producer</source>
        <translation>Hersteller</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="133"/>
        <source>Product</source>
        <translation>Produkt</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="232"/>
        <source>Import</source>
        <translation>Importieren</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="249"/>
        <source>Write Logfile</source>
        <translation>Logdatei schreiben</translation>
    </message>
    <message>
        <location filename="../../DialogImport.ui" line="275"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="19"/>
        <source>Loading plugin translation failed!</source>
        <translation>Laden der Erweiterung Übersetzung fehlgeschlagen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="55"/>
        <source>No bluetooth controller found!</source>
        <translation>Kein Bluetooth-Kontroller gefunden!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="101"/>
        <source>Bluetooth error!

%1</source>
        <translation>Bluetooth Fehler!

%1</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="148"/>
        <source>Selected device doesn&apos;t look like %1!</source>
        <translation>Das ausgewählte Gerät sieht nicht nach %1 aus!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="247"/>
        <source>No answer from device!

Did you use the bluetooth controller with the cloned mac address?</source>
        <translation>Keine Antwort vom Gerät!

Wurde der Bluetooth-Controller mit der geklonten Mac-Adresse verwendet?</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="378"/>
        <source>Can&apos;t access bluetooth service %1!</source>
        <translation>Kann nicht auf den Bluetooth-Dienst %1 zugreifen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="491"/>
        <source>The selected bluetooth controller is not available!</source>
        <translation>Der ausgewählte Bluetooth-Kontroller ist nicht verfügbar!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="535"/>
        <source>Could not open logfile %1!

%2</source>
        <translation>Konnte Logdatei %1 nicht öffnen!

%2</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="332"/>
        <source>Import aborted by user!</source>
        <translation>Import durch Benutzer abgebrochen!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="116"/>
        <source>No device discovered!

Check the operating system connection state or press bluetooth button on device and try again…</source>
        <translation>Kein Gerät gefunden!

Den Verbindungsstatus des Betriebssystems überprüfen oder die Bluetooth-Taste am Gerät drücken und erneut versuchen…</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="166"/>
        <source>Could not connect to device!

Check the operating system connection state or press bluetooth button on device and try again…

%1</source>
        <translation>Konnte keine Verbindung zum Gerät herstellen!

Den Verbindungsstatus des Betriebssystems überprüfen oder die Bluetooth-Taste am Gerät drücken und erneut versuchen…

%1</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="569"/>
        <source>Import in progress!</source>
        <translation>Import wird durchgeführt!</translation>
    </message>
    <message>
        <location filename="../../DialogImport.cpp" line="558"/>
        <source>Really abort import?</source>
        <translation>Import wirklich abbrechen?</translation>
    </message>
</context>
</TS>
