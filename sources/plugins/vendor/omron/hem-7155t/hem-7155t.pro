TEMPLATE	= lib
CONFIG		+= plugin no_plugin_name_prefix
QT			+= widgets bluetooth
DEFINES		+= HEM7155T
INCLUDEPATH	+= ../../../../
SOURCES		= DialogImport.cpp ../../../shared/plugin/plugin.cpp
HEADERS		= DialogImport.h   ../../../shared/plugin/plugin.h
FORMS		= DialogImport.ui
RESOURCES	= res/hem-7155t.qrc
TARGET		= ../../../omron-hem7155t

unix:!macx {
	QMAKE_RPATHDIR += $ORIGIN/../../lib
}

win32 {
CONFIG		-= debug_and_release
}

macx {
}

system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../hem-7361t/DialogImport.* .))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../hem-7361t/res/qm/hem-7361t-de_DE.qm res/qm/hem-7155t-de_DE.qm))
system($$QMAKE_COPY_FILE $$shell_path($$OUT_PWD/../../../shared/plugin/svg/*.svg res/svg))
QMAKE_CLEAN += $$OUT_PWD/DialogImport.*
QMAKE_CLEAN += $$OUT_PWD/res/qm/*.qm
QMAKE_CLEAN += $$OUT_PWD/res/svg/*.svg
