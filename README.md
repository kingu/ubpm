# **UBPM - Universal Blood Pressure Manager**

[![](https://badgen.net/badge/Framework/Qt5/orange)](https://qt.io "Qt Info")
[![](https://badgen.net/badge/Platform/WIN%20|%20LIN%20|%20MAC/blue)](https://codeberg.org/lazyt/ubpm/releases "Download Release")
[![](https://badgen.net/badge/License/GPL-3.0/pink)](https://codeberg.org/lazyt/ubpm/src/branch/master/LICENSE "Show License")
[![](https://badgen.net/badge/Paypal/Sponsor%20Beer%20Once/red)](https://paypal.me/lazyt "Support Developer")
[![](https://badgen.net/badge/Liberapay/Sponsor%20Beer%20Weekly/red)](https://liberapay.com/lazyt/donate "Support Developer")
[![](https://hosted.weblate.org/widgets/ubpm/-/svg-badge.svg)](https://hosted.weblate.org/engage/ubpm "Contribute Translation")

## Description

Tired of original software supplied by the manufacturer of your blood pressure monitor because it's only available for Windows and requires an internet connection for uploading your private health data into the cloud? Then try UBPM for free and use it on Windows, Linux and MacOS!

The current version supports the following features:

* import data from manual input, file (csv, json, xml, sql) or directly from supported blood pressure monitors
* export data to csv, json, xml or sql format
* view, print and mail data as chart, table or statistics
* analyze data via sql queries
* plugin interface for blood pressure monitors with computer interface
* online updater
* context sensitive help via F1 key
* style gui via css files
* multi language (EN, DE, ES, NL, NO, RU)
* cross platform (same look & feel on Windows, Linux, MacOS)

#### Supported blood pressure monitors

Manufacturer | Model | Description | Interface | Comment
:---:|:---:|:---:|:---:|:---:
[OMRON](https://omronhealthcare.com/blood-pressure) | HEM-7361T | M7/M500 Intelli IT, X7 Smart | BLUETOOTH | [Important!](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/omron/hem-7361t/readme.md)<br>Thanks to "Elwood" for sponsoring the device
[OMRON](https://omronhealthcare.com/blood-pressure) | HEM-7155T | M4/M400 Intelli IT, X4 Smart | BLUETOOTH | [Important!](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/omron/hem-7361t/readme.md)<br>Thanks to Annett Heyder for testing
[OMRON](https://omronhealthcare.com/blood-pressure) | HEM-7322U | M6 Comfort IT, M500 IT | USB (HID) |
[OMRON](https://omronhealthcare.com/blood-pressure) | HEM-7131U | M3 IT, M400 IT | USB (HID) |
[OMRON](https://omronhealthcare.com/blood-pressure) | HEM-7080IT | M10 IT, M9 Premium | USB (HID) | Thanks to Sven Schirrmacher for giving access via usbip over internet
[OMRON](https://omronhealthcare.com/blood-pressure) | HEM-730xIT | MIT Elite (Plus) | USB (HID) | Thanks to Helio Machado for giving access via usbip over internet
[HARTMANN](https://www.veroval.info/en/products/bloodpressure) | BPM25 | Veroval Wrist | USB (SERIAL) | Thanks to Carsten Presser for testing
[HARTMANN](https://www.veroval.info/en/products/bloodpressure) | DC318 | Veroval Duo Control | USB (SERIAL) | Untested, please report
[HARTMANN](https://www.veroval.info/en/products/bloodpressure) | GCE604 | Veroval Upper Arm | USB (SERIAL) | Thanks to Annett Heyder for testing

Please help to add more devices! Read the "Device Plugins" section of the guide and take a look at the plugin sources ([usb-hid](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/omron/hem-7322u), [usb-serial](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/hartmann/gce604), [bluetooth](https://codeberg.org/LazyT/ubpm/src/branch/master/sources/plugins/vendor/omron/hem-7361t)) to get started.

## Screenshots

Main window with some records...

![Chart View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/chartview.png "Chart View")
![Table View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/tableview.png "Table View")
![Stats View](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/statsview.png "Stats View")

Print or mail records...

![Chart Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/chartprint.png "Chart Print")
![Table Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/tableprint.png "Table Print")
![Stats Print](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/statsprint.png "Stats Print")

Create or modify records...

![Input](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/input.png "Input")

Import from supported device...

![Import](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/import.gif "Import")

Analyze records via sql queries...

![Analysis](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/analysis.png "Analysis")

Configure your preferred settings...

![Settings](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/settings.png "Settings")

Press F1 to get help for the current context...

![Guide](https://codeberg.org/lazyt/ubpm/raw/branch/master/website/guide.png "Guide")

## Download

Download the latest version for your operating system. All 3 files (exe, dmg, AppImage) contain everything to run UBPM on the target platform without installing anything.

* [Windows (exe)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is an [7zip](https://www.7-zip.org) self extracting archive. It will be automatically extracted to "%temp%\7zxxxxxxxx" and after that the "ubpm.exe" is started. You can copy this directory or extract the file if you want the content.

**IMPORTANT NOTE: some virus scanners classify UBPM as malicious! This is due to the use of [7zsfx](https://www.7-zip.org/a/lzma1900.7z). If you don't trust me rename exe to zip, unpack and scan that directory, no warnings should appear for UBPM itself.**

* [Linux (AppImage)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is an [AppImage](https://appimage.org) package. Don't forget to "chmod +x *.AppImage" after download and execute. You can use the parameter "--appimage-extract" if you want the content.

For better system integration it's recommended to install [AppImageD](https://github.com/AppImage/appimaged) or [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher).

* [MacOS (dmg)](https://codeberg.org/LazyT/ubpm/releases/latest)

This is an Apple disc image. You can mount and run or copy the application.

### Verify Download

To verify the download was not tampered, a corresponding \*.sig file is also available. It contains the hash and the signature of the release.

Two external commands are needed to check the integrity and authenticity: "sha256sum" and "minisign". If these are missing on your system please install first:

**sha256sum**

* Windows: download [busybox](https://frippery.org/files/busybox/busybox.exe) and save as "sha256sum.exe"
* Linux: install via package manager, e.g. ```apt install coreutils```
* MacOS: install via [homebrew](https://brew.sh), e.g. ```brew install coreutils```

**minisign**

* Windows: download and unzip [minisign-win.zip](https://github.com/jedisct1/minisign/releases/latest)
* Linux: install via package manager, use [ppa](https://launchpad.net/~dysfunctionalprogramming/+archive/ubuntu/minisign) or compile from [source](https://github.com/jedisct1/minisign/archive/refs/heads/master.zip)
* MacOS: download and unzip [minisign-osx.zip](https://github.com/jedisct1/minisign/releases/latest) or install via package manager, e.g. ```brew install minisign```

After downloading the release and signature files open a terminal and copy & paste the commands found in the \*.sig comment sections.

Example for Linux 1.0.4:

```echo 344e9fe0882d0fdb3057d094cfa6d986e7cbda7d1b23d5f2b734365b5549fce8  ubpm-1.0.4.AppImage | sha256sum -c```

verified -> "ubpm-1.0.4.AppImage: OK"\
failed -> "ubpm-1.0.4.AppImage: FAILED"

```minisign -V -P RWRjSAJt4zCGy2SYhAWoLpygWZv5640wfhLqi/NLnaznnu18+txes5YA -m ubpm-1.0.4.AppImage -x ubpm-1.0.4.AppImage.sig```

verified -> "Signature and comment signature verified"\
failed -> "(Comment) Signature verification failed"

If you get a failed result don't use the downloaded file! Maybe it was only corrupted during download, but it's also possible that it was intentionally manipulated to harm you.

The public signing key for UBPM is always **RWRjSAJt4zCGy2SYhAWoLpygWZv5640wfhLqi/NLnaznnu18+txes5YA** for all releases.

## Build from Source

Download the latest [Qt Online Installer](https://qt.io/download-open-source) and install Qt 5.15.x, Qt Charts and OpenSSL binaries first.

- Checkout the source code via git (or download and extract the [zip](https://codeberg.org/LazyT/ubpm/archive/master.zip))

		git clone https://codeberg.org/lazyt/ubpm

- Change into the ubpm sources directory and generate the Makefile

		cd ubpm/sources && qmake

- Compile the source code

		make

- Optional create a release package (installs nothing on the build system)

		make install

## Add new Localization

If your preferred language is missing please contribute a translation. There are 2 ways of doing this:

### Online

The easiest way is to translate everything online via [Weblate](https://hosted.weblate.org/engage/ubpm), but you need to register a free account first.

Open the [project page](https://hosted.weblate.org/projects/ubpm) and select the component you would like to start with, e.g. "Application". Click on "Start new translation" and set your language (or select an already existing but unfinished language to complete), translate all strings and repeat with the next component, e.g. "Guide" and "Plugin".

It can be helpful to open the "\*.ts" file in "linguist", because you can see the context of the string in a real-time view of the user interface.

You can download all components via "Files -> Download ZIP" and test it like described under the Local section below, **but skip:**

* application/plugin: all steps and only run ```lrelease *.ts``` instead to create the "*.qm" files
* guide: the step copying "en.json" and use your downloaded "xx.json" instead

### Local

The "xx" in the following text stands for your 2 digit [ISO-639-1](https://www.loc.gov/standards/iso639-2/php/code_list.php) language code, for example "en" for English.

The commands "lrelease", "lupdate" and "linguist" are part of the [Qt Framework](https://www.qt.io/download-qt-installer). Standalone binaries are also available for [Windows](https://github.com/thurask/Qt-Linguist/releases). For Linux/MacOS use the package manager of your distribution and search for something like "qttools5-dev-tools".

A complete localization consists of 3 parts:

**1. Application**

To translate the app go to "sources/mainapp" and

* find and add "TRANSLATIONS ... lng/ubpm_xx.ts" in "mainapp.pro"
* run ```lupdate -no-obsolete mainapp.pro``` to create the "ubpm_xx.ts" with English strings
* run ```linguist lng/ubpm_xx.ts``` and translate all strings
* click on "File\Save" and "File\Release"
* new file "ubpm_xx.qm" should be created and contains the app localization
* to test your new app translation copy this file to your "UBPM/Languages" directory

**2. Plugins**

To translate the plugin(s) go to "sources/plugins/vendor" and

* search all "\*.pro" with "TRANSLATIONS ..." (\* stands for device names like "hem-7322u")
* find and add "TRANSLATIONS ... res/qm/\*-xx_XX.ts" in "\*.pro"
* add ```<file>qm/*-xx_XX.qm</file>``` in "res/\*.qrc"
* run ```lupdate -no-obsolete *.pro``` to create the "\*-xx_XX.ts" with English strings
* run ```linguist res/qm/*-xx_XX.ts``` and translate all strings
* click on "File\Save" and "File\Release"
* new file "\*-xx_XX.qm" should be created and contains the plugin localization
* to test your new plugin translation recompile and copy the plugin to your "UBPM/Plugins" directory

**3. Guide**

To translate the guide go to "sources/mainapp/hlp" and

* copy "help/en.qh\*" to "help/xx.qh\*"
* change ```<file>../en.qch</file>``` in "xx.qhcp" to ```<file>../xx.qch</file>```
* change ```<virtualFolder>en</virtualFolder>``` in "xx.qhp" to ```<virtualFolder>xx</virtualFolder>``` and translate all section titles
* copy "html/lang/en.json" to "html/lang/xx.json" and translate all strings
* copy "html/img/en" to "html/img/xx" and replace all images with your localized screenshots
* find and add "for LNG ... xx; do" in "gen-help.sh"
* run this script to build the "xx" guide (requires static-i18n installed)
* new files "xx.qch" and "xx.qhc" should be created and contains the localized guide
* to test your new guide translation copy these files to your "UBPM/Guides" directory

Now start UBPM, switch to your new language and test everything...

## Credits

UBPM is based on

* [Qt](https://www.qt.io)
* [Weblate](https://weblate.org)
* [hidapi](https://github.com/libusb/hidapi)
* [libomron](https://github.com/openyou/libomron/blob/master/doc/omron_protocol_notes.asciidoc)
* [veroval](https://github.com/cpresser/veroval/blob/master/veroval.py)

Thanks for this great software! :+1:
